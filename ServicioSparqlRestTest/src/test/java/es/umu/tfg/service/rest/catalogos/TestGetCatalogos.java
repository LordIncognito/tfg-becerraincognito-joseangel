package es.umu.tfg.service.rest.catalogos;


import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.config.ConnectionConfig;
import static io.restassured.RestAssured.*;

public class TestGetCatalogos {

	private static final String URL_SERVICIO_REST = "ServicioSparqlREST";

	@BeforeClass
	public static void setup() {
		RestAssured.baseURI = RestAssured.baseURI + ":8080/" + URL_SERVICIO_REST + "/graphs/catalogues";
		RestAssured.config = RestAssured.config()
				.connectionConfig(ConnectionConfig.connectionConfig()
						.closeIdleConnectionsAfterEachResponseAfter(2000, TimeUnit.MILLISECONDS));
	}
	
	
	@Test
	public void testGetCatalogoUltimaVersion() {
		String titulo = "Moleculas";
		given()
		.get("/"+ titulo)
		.then()
		.contentType("application/rdf+xml")
		.statusCode(200);
	}
	
	@Test
	public void testGetCatalogoVersionAnterior() {
		String titulo = "Moleculas";
		given()
		.queryParam("version", "V2")
		.get("/"+ titulo)
		.then()
		.contentType("application/rdf+xml")
		.statusCode(200);
	}
	
	
}
