package es.umu.tfg.service.rest.consultas;

import static io.restassured.RestAssured.given;

import java.io.File;

import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;

public class TestPostServicioSPARQLRest {

	private static final String URL_SERVICIO_REST = "ServicioSparqlREST";

	@BeforeClass
	public static void setup() {
		RestAssured.baseURI = RestAssured.baseURI + ":8080/" + URL_SERVICIO_REST + "/graphs";
		File dir = new File("config");
		File[] files = dir.listFiles();
		if (files != null) {
			int length = files.length;
			for (int i = 0; i < length; i++)
				files[i].delete();
		}
	}

	@Test
	public void testPostDBpedia() {
		String graph = "http://dbpedia.org";
		String server = "https://dbpedia.org/sparql";
		given()
		.formParam("graph", graph)
		.formParam("server", server)
		.post()
		.then()
		.statusCode(201);
	}

	@Test
	public void testPostMolecule() {
		String graph = "http://sele.inf.um.es/molecule";
		String server = "http://sele.inf.um.es/sparql";
		given()
		.formParam("graph", graph)
		.formParam("server", server)
		.post()
		.then()
		.statusCode(201);
	}

	@Test
	public void testPostColorectalDomain() {
		String graph = "http://sele.inf.um.es/colorectal-domain";
		String server = "http://sele.inf.um.es/sparql";
		given()
		.formParam("graph", graph)
		.formParam("server", server)
		.post()
		.then()
		.statusCode(201);
	}

}
