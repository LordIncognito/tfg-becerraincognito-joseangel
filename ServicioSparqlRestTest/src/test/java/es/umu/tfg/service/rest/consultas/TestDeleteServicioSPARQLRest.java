package es.umu.tfg.service.rest.consultas;

import static io.restassured.RestAssured.given;


import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;

public class TestDeleteServicioSPARQLRest {
	private static final String URL_SERVICIO_REST = "ServicioSparqlREST";
	
	@BeforeClass
	public static void setup() {
		RestAssured.baseURI = RestAssured.baseURI + ":8080/" + URL_SERVICIO_REST + "/graphs";
	}
	
	@Test
	public void testDeleteByName() {
		String graph = "molecule";
		given().formParam("graph", graph).delete().then().statusCode(204);
	}
	
	@Test
	public void testDeleteByURL() {
		String graph = "http://dbpedia.org";
		given().formParam("graph", graph).delete().then().statusCode(204);
	}

}
