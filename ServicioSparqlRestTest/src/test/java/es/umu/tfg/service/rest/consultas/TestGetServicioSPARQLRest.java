package es.umu.tfg.service.rest.consultas;


import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.config.ConnectionConfig;

import static io.restassured.RestAssured.*;

import java.util.concurrent.TimeUnit;

public class TestGetServicioSPARQLRest {

	private static final String URL_SERVICIO_REST = "ServicioSparqlREST";

	@BeforeClass
	public static void setup() {
		RestAssured.baseURI = RestAssured.baseURI + ":8080/" + URL_SERVICIO_REST + "/graphs";
		RestAssured.config = RestAssured.config()
				.connectionConfig(ConnectionConfig.connectionConfig()
						.closeIdleConnectionsAfterEachResponseAfter(2000, TimeUnit.MILLISECONDS));
		
		/*RestAssured.config = RestAssured.config()
				.httpClient(HttpClientConfig.httpClientConfig()
						.reuseHttpClientInstance());*/


	}
	

	@Test
	public void testGetMostrarGrafosATOM() {
		given().get().then().statusCode(200);
	}

	@Test
	public void testGetMostrarGrafosHAL() {
		given().queryParam("format", "application/hal+json")
		.get()
		.then()
		.contentType("application/hal+json")
		.statusCode(200);
	}

	@Test
	public void testGetMostrarConsultasATOM() {
		given().get("/molecule").then().statusCode(200);
	}

	@Test
	public void testGetMostrarConsultasATOMSorted() {
		given().queryParam("sorted", true).get("/molecule").then().statusCode(200);
	}

	@Test
	public void testGetMostrarConsultasATOMLimit() {
		given().queryParam("limit", 10).get("/molecule").then().statusCode(200);
	}

	@Test
	public void testGetMostrarConsultasATOMOffset() {
		given().queryParam("offset", 10).get("/molecule").then().statusCode(400);
	}

	@Test
	public void testGetMostrarConsultasATOMOLimitAndffset_9() {
		given().queryParam("limit", 10).queryParam("offset", 10)
		.get("/molecule").then().statusCode(200);
	}

	@Test
	public void testGetMostrarConsultasTURTLE() {
		given()
		.queryParam("format", "text/turtle")
		.get("/molecule")
		.then()
		.contentType("text/turtle")
		.statusCode(200);
	}

	@Test
	public void testGetMostrarConsultasTURTLESorted() {
		given().queryParam("sorted", true)
		.queryParam("format", "text/turtle")
		.get("/molecule").then().statusCode(200);
	}

	@Test
	public void testGetMostrarConsultasTURTLELimit() {
		given().queryParam("limit", 20)
		.queryParam("format", "text/turtle")
		.get("/molecule").then().statusCode(200);
	}

	@Test
	public void testGetMostrarConsultasTURTLEOffset() {
		given().queryParam("offset", 5)
		.queryParam("format", "text/turtle")
		.get("/molecule").then().statusCode(400);
	}

	@Test
	public void testGetMostrarConsultasTURTLEShortedAndLimit() {
		given().queryParam("shorted", true)
		.queryParam("limit", 5)
		.queryParam("format", "text/turtle")
		.get("/molecule")
		.then().statusCode(200);
	}

	@Test
	public void testGetMostrarConsultasHTML() {
		given()
		.queryParam("format", "text/html")
		.get("/molecule")
		.then()
		.contentType("text/html")
		.statusCode(200);
	}
	
	@Test
	public void testGetMostrarFiltrado() {
		given()
		.urlEncodingEnabled(false)
		.queryParam("filter", "regex=http://www.w3.org/2000/01/rdf-schema%23label=C1")
		.get("/molecule/classes/http://miuras.inf.um.es/ontologies/molecule.owl%23Atom")
		.then()
		.contentType("text/turtle")
		.statusCode(200);
	}
	
	@Test
	public void testGetMostrarFiltradoRDF() {
		given()
		.urlEncodingEnabled(false)
		.queryParam("filter", "regex=http://www.w3.org/2000/01/rdf-schema%23label=C1")
		.queryParam("format", "application/rdf%2bxml")
		.get("/molecule/classes/http://miuras.inf.um.es/ontologies/molecule.owl%23Atom")
		.then()
		.contentType("application/rdf+xml")
		.statusCode(200);
	}
	

}
