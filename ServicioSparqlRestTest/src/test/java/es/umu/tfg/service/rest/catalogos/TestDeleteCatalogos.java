package es.umu.tfg.service.rest.catalogos;

import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.config.ConnectionConfig;
import static io.restassured.RestAssured.*;

public class TestDeleteCatalogos {
	private static final String URL_SERVICIO_REST = "ServicioSparqlREST";

	@BeforeClass
	public static void setup() {
		RestAssured.baseURI = RestAssured.baseURI + ":8080/" + URL_SERVICIO_REST + "/graphs/catalogues";
		RestAssured.config = RestAssured.config()
				.connectionConfig(ConnectionConfig.connectionConfig()
						.closeIdleConnectionsAfterEachResponseAfter(2000, TimeUnit.MILLISECONDS));

	}
	
	@Test
	public void testDeleteCatalogue() {
		String titulo = "Moleculas";
		given()
		.formParam("title", titulo)
		.delete()
		.then()
		.statusCode(204);
		
	}
}
