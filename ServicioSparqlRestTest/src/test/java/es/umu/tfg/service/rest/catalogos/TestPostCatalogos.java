package es.umu.tfg.service.rest.catalogos;


import java.io.File;
import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.config.ConnectionConfig;
import static io.restassured.RestAssured.*;
public class TestPostCatalogos {

	private static final String URL_SERVICIO_REST = "ServicioSparqlREST";

	@BeforeClass
	public static void setup() {
		RestAssured.baseURI = RestAssured.baseURI + ":8080/" + URL_SERVICIO_REST + "/graphs/catalogues";
		RestAssured.config = RestAssured.config()
				.connectionConfig(ConnectionConfig.connectionConfig()
						.closeIdleConnectionsAfterEachResponseAfter(2000, TimeUnit.MILLISECONDS));

	}

	@Test
	public void testCatalogoValido() {
		File catalogue = new File("catalogues/CatalogoValido.rdf");
		given()
		.multiPart("catalogue", catalogue)
		.post()
		.then()
		.statusCode(201);
	}
	
	@Test
	public void testCatalogoValidoVersionDiferente() {
		File catalogue = new File("catalogues/CatalogoValidoV2.rdf");
		given()
		.multiPart("catalogue", catalogue)
		.post()
		.then()
		.statusCode(201);
	}


	
	@Test
	public void testCatalogoInvalido() {
		File catalogue = new File("catalogues/CatalogoInvalido.rdf");
		given()
		.multiPart("catalogue", catalogue)
		.post()
		.then()
		.statusCode(400);
	}
}
