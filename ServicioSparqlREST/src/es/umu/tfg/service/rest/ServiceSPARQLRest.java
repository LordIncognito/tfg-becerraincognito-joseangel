package es.umu.tfg.service.rest;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import org.w3c.dom.Document;

import es.umu.tfg.service.formats.CustomFormats;

import es.umu.tfg.sparql.controller.ServiceSPARQL;
import es.umu.tfg.sparql.parameters.Resource;
import es.umu.tfg.sparql.queryBuilder.OperationType;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

/**
 * Servicio REST
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
@Path("graphs")
public class ServiceSPARQLRest {
	@Context
	private UriInfo uriInfo;
	private ServiceSPARQL controlador = ServiceSPARQL.getUniqueInstance();
	/**
	 * Expresion regular para las consultas basicas
	 */
	private static final String REGULAR_EXPRESSION_INSTANCES_PATH = "((https?)://[-a-zA-Z0-9+&@#/%?=~_|!\\(\\):,.;]*[-a-zA-Z0-9+&@#/%=~_\\(\\)|]|[a-zA-Z0-9_#-]+)";
	/**
	 * Expresion regular para los DESCRIBE
	 */
	private static final String REGULAR_EXPRESSION_DESCRIBE_PATH = "(https?)://[-a-zA-Z0-9+&@#/%?=~_|!\\(\\):,.;]*[-a-zA-Z0-9+&@#/%=~_\\(\\)|]";

	private static final String DESCRIBE_VALID_FORMATS[] = { "text/turtle", "text/x-html-nice-turtle",
			"application/rdf+json", "application/atom+xml", "text/x-html+tr", "text/x-html+ul",
			"text/x-html-script-ld+json", "text/x-html-script-turtle", "application/odata+json",
			"application/x-json+ld", "application/xhtml+xml", "application/x-nice-microdata" };

	/**
	 * Array donde definimos los formatos aceptados y que soporta el servidor
	 */
	private static final String VALID_FORMATS[] = concatArrays(new String[] { "application/xml", "application/json",
			"application/rdf+xml", "text/html", "application/hal+json", "application/javascript", "n-triples",
			"text/csv", "text/tab-separated-values" }, DESCRIBE_VALID_FORMATS);

	/**
	 * Array para formatos especiales
	 */
	private static final String SPECIAL_FORMATS[] = { "text/turtle", "text/plain", "text/x-html-nice-turtle",
			"application/json", "application/javascript", "n-triples", "text/csv", "text/tab-separated-values",
			"application/rdf+json", "application/atom+xml", "application/odata+json", "application/x-json+ld",
			"application/x-trig", "application/x-nice-microdata", "text/x-html-script-ld+json",
			"text/x-html-script-turtle" };

	/**
	 * Concatena dos arrays
	 * 
	 * @param a1
	 *            array1
	 * @param a2
	 *            array2
	 * @return array
	 */
	private static String[] concatArrays(String[] a1, String[] a2) {
		int a1Length = a1.length;
		int a2Length = a2.length;
		int lengthTotal = a1Length + a2Length;
		String[] newArray = new String[lengthTotal];
		System.arraycopy(a1, 0, newArray, 0, a1Length);
		System.arraycopy(a2, 0, newArray, a1Length, a2Length);
		return newArray;
	}

	private boolean isCountInstancesByClass() {
		String path = uriInfo.getAbsolutePath().toString();
		return path.contains("classes") && path.contains("count");
	}

	private boolean isCountInstancesByProperty() {
		String path = uriInfo.getAbsolutePath().toString();
		return path.contains("properties") && path.contains("count");
	}

	/**
	 * Método para comprobar si el format solicitado es soportado por el servicio
	 * 
	 * @param format
	 *            format solicitado por el cliente
	 * @return true/false
	 */
	private boolean isValidFormat(String format) {
		for (String formatValido : VALID_FORMATS) {
			if (format.equals(formatValido))
				return true;
		}

		return false;
	}

	/**
	 * Método para comprobar si se ha solicitado formats que no siguen un sistema
	 * de etiquetas como: turtle, JSON, TSV, etc.
	 * 
	 * @param format
	 *            format solicitado
	 * @return format especial
	 */
	private boolean isSpecialFormat(String format) {
		for (String formatEspecial : SPECIAL_FORMATS) {
			if (format.equals(formatEspecial))
				return true;
		}
		return false;
	}

	/**
	 * Método para crear un Response de error
	 * 
	 * @param code
	 *            Codigo de error
	 * @param msg
	 *            Mensaje de error
	 * @return Response
	 */
	private Response createErrorResponse(Status code, String msg) {
		return Response.status(code).type(MediaType.APPLICATION_XML).entity("<error> " + msg + "</error>").build();
	}

	/**
	 * Método para crear el objeto response para los formats TURTE y JSON
	 * 
	 * @param resource
	 *            Parametros recibidos por el servicio
	 * @return objeto response
	 */
	private Response createSpecialResponse(Resource resource) {
		String respuestaControlador = "";
		OperationType operacion = null;

		// Analizamos el tipo de operacion a realizar
		// Si se hace acceso mediante DESCRIBE entonces es una operacion de DESCRIPCION
		// en caso contrario se comprueba si consulta de clases o de propiedades
		operacion = (uriInfo.getAbsolutePath().toString().contains("describe") ? OperationType.DESCRIBE_ELEMENT
				: (uriInfo.getAbsolutePath().toString().contains("classes") ? OperationType.GET_ELEMENTS_BY_CLASS
						: OperationType.GET_ELEMENTS_BY_PROPERTY));

		if (this.isCountInstancesByClass())
			operacion = OperationType.GET_COUNT_BY_CLASS;

		if (this.isCountInstancesByProperty())
			operacion = OperationType.GET_COUNT_BY_PROPERTY;

		// Si se esta haciendo una consulta de <URI_propiedad, valor>
		if (!this.isStringNullOrEmpty(resource.getValue())) {
			operacion = OperationType.GET_ELEMENTS_BY_VALUE;
		}

		if (resource.getUrl().endsWith("/"))
			resource.setUrl(resource.getUrl().substring(0, resource.getUrl().length() - 1));

		respuestaControlador = controlador.querySPARQL(operacion, true, resource);

		return Response.status(Status.OK).entity(respuestaControlador).type(resource.getFormat()).build();
	}

	private boolean isSortedFilter(boolean orden, String filter, String sortedBy) {
		return orden && !filter.isEmpty() && !sortedBy.isEmpty();
	}

	/**
	 * Comprueba si el formato del DESCRIBE es valido
	 * 
	 * @param format
	 *            formato solicitado
	 * @return true/false
	 */
	private boolean isValidDescribeFormat(String format) {
		for (String describeFormat : DESCRIBE_VALID_FORMATS)
			if (describeFormat.equals(format))
				return true;

		return false;
	}

	/**
	 * Retorna un listado de los formatos soportados por la API
	 * 
	 * @return
	 */
	private String listFormats() {
		String formatos = "<formats>";
		// Se pasa a un SET para evitar repeticiones
		Set<String> formatsSupported = new HashSet<String>(Arrays.asList(VALID_FORMATS));
		for (String formato : formatsSupported) {
			if (!formato.equals("text/turtle") && this.isValidDescribeFormat(formato))
				formato += " (format only available in describe path, /describe/\"element\")";
			formatos += "<format>" + formato + "</format>";
		}
		formatos += "</formats>";
		return formatos;
	}

	/**
	 * Obtiene un listado de los formatos DESCRIBE soportados
	 * 
	 * @return listado formatos DESCRIBE
	 */
	private String listDescribeFormats() {
		String formatos = "<formats>";
		for (String formato : DESCRIBE_VALID_FORMATS) {
			formatos += "<format>" + formato + "</format>";
		}
		formatos += "</formats>";
		return formatos;
	}

	/**
	 * Método para crear el objeto response correspondiente a la petición según
	 * los parametros
	 * 
	 * @param resource
	 *            Parametros recibidos por el servicio
	 * @return objeto response
	 */
	private Response createResponse(Resource resource) {

		if (this.isSpecialFormat(resource.getFormat())) {
			return this.createSpecialResponse(resource);
		}
		OperationType operacion = null;
		Document resultado = null;
		operacion = (uriInfo.getAbsolutePath().toString().contains("describe") ? OperationType.DESCRIBE_ELEMENT
				: (uriInfo.getAbsolutePath().toString().contains("classes") ? OperationType.GET_ELEMENTS_BY_CLASS
						: OperationType.GET_ELEMENTS_BY_PROPERTY));

		if (this.isCountInstancesByClass())
			operacion = OperationType.GET_COUNT_BY_CLASS;

		if (this.isCountInstancesByProperty())
			operacion = OperationType.GET_COUNT_BY_PROPERTY;

		if (!this.isStringNullOrEmpty(resource.getValue()))
			operacion = OperationType.GET_ELEMENTS_BY_VALUE;

		if (resource.getUrl().endsWith("/"))
			resource.setUrl(resource.getUrl().substring(0, resource.getUrl().length() - 1));

		resultado = controlador.querySPARQL(operacion, resource);

		return Response.status(Status.OK).entity(resultado).type(resource.getFormat()).build();

	}

	/**
	 * Método para obtener el nombre del grafo del PATH actual
	 * 
	 * @return grafo
	 */
	private String getGrafoPath() {
		String path = uriInfo.getPath();
		int posStart = path.indexOf("graphs");
		if (posStart < 0)
			return "";
		posStart = path.indexOf("/", posStart);
		if (posStart < 0)
			return "";
		int posEnd = path.indexOf("/", posStart + 1);
		if (posEnd < 0)
			return "";
		return path.substring(posStart + 1, posEnd);
	}

	private boolean isValidFormatEntry(String format) {
		return format.equals("application/atom+xml") || format.equals("application/hal+json");
	}

	private Response getGraphMenu(Resource resource) {
		String formats = "<formats>" + "<format>application/atom+xml</format>"
				+ "<format>application/hal+json</format>";
		if (!this.isValidFormatEntry(resource.getFormat()))
			return this.createErrorResponse(Status.NOT_ACCEPTABLE, "Format not supported." + formats);

		return Response.status(Status.OK).entity(controlador.graphsMenu("ServicioSparqlREST",
				uriInfo.getAbsolutePath().toString(), resource.getFormat())).type(resource.getFormat()).build();

	}

	private Response getMenuDataOfGraph(Resource resource) {

		String grafo = resource.getGraph();
		if (uriInfo.getPath().contains("properties") || uriInfo.getPath().contains("classes")) {
			grafo = this.getGrafoPath();
			if (this.isStringNullOrEmpty(grafo))
				return createErrorResponse(Status.BAD_REQUEST, "Path incompleted");
		} else if (grafo.endsWith("/"))
			grafo = grafo.substring(0, grafo.length() - 1);

		resource.setGraph(grafo);
		if (!this.isValidFormatEntry(resource.getFormat())) {
			boolean specialFormat = this.isSpecialFormat(resource.getFormat());
			if (uriInfo.getPath().contains("classes"))
				return (specialFormat
						? Response.status(Status.OK)
								.entity(controlador.querySPARQL(OperationType.GET_CLASSES, true, resource))
								.type(resource.getFormat()).build()
						: Response.status(Status.OK)
								.entity(controlador.querySPARQL(OperationType.GET_CLASSES, resource))
								.type(resource.getFormat()).build());

			if (uriInfo.getPath().contains("properties"))
				return (specialFormat
						? Response.status(Status.OK)
								.entity(controlador.querySPARQL(OperationType.GET_PROPERTIES, true, resource))
								.type(resource.getFormat()).build()
						: Response.status(Status.OK)
								.entity(controlador.querySPARQL(OperationType.GET_PROPERTIES, resource))
								.type(resource.getFormat()).build());

			return (specialFormat
					? Response.status(Status.OK).entity(controlador.querySPARQL(OperationType.GET_ALL, true, resource))
							.type(resource.getFormat()).build()
					: Response.status(Status.OK).entity(controlador.querySPARQL(OperationType.GET_ALL, resource))
							.type(resource.getFormat()).build());
		}

		return Response.status(Status.OK)
				.entity(controlador.graphDataMenu("ServicioSparqlREST", uriInfo.getAbsolutePath().toString(), grafo,
						VALID_FORMATS, DESCRIBE_VALID_FORMATS, resource))
				.type(resource.getFormat()).type(resource.getFormat()).build();
	}

	/**
	 * Método para crear el objeto response correspondiente a la petición según
	 * los parametros
	 * 
	 * @param resource
	 *            Parametros recibidos por el servicio
	 * @return objeto response
	 */
	private Response getResponse(Resource resource) {

		if (!this.isValidFormat(resource.getFormat()))
			return createErrorResponse(Status.NOT_ACCEPTABLE,
					"The requested format is not supported" + this.listFormats());

		if (resource.getLimit() < 0 && resource.getOffset() > 0)
			return createErrorResponse(Status.BAD_REQUEST, "You can´t use offset without limit");

		if (uriInfo.getAbsolutePath().toString().contains("describe")
				&& !this.isValidDescribeFormat(resource.getFormat()))
			return createErrorResponse(Status.NOT_ACCEPTABLE, "That format is only available in describe path");

		if (resource.getFormat().equals("n-triples"))
			resource.setFormat("text/plain");

		// Miramos si debemos imprimir el menu de ayuda
		if (this.isStringNullOrEmpty(resource.getGraph())) {
			return getGraphMenu(resource);

		} else if (this.isStringNullOrEmpty(resource.getUrl())) {
			return getMenuDataOfGraph(resource);
		}

		if (!resource.isSorted() && !resource.getSortedBy().equals("tipo"))
			return createErrorResponse(Status.BAD_REQUEST, "You must activate the flag \"sorted\" (sorted=true)");

		// Si es una consulta sin filtrado ni ordenacion pues se hace una peticion
		// normal
		if (this.isSortedFilter(resource.isSorted(), resource.getFilter(), resource.getSortedBy())) {

			if (resource.isSorted() && resource.getSortedBy().equals("tipo"))
				return this.createResponse(resource);

			if (resource.getSortedBy().isEmpty())
				resource.setSortedBy("tipo");

			return this.createResponse(resource);
			// Si es una consulta con filtro y con criterios de ordenacion
		}

		return this.createResponse(resource);

	}

	private boolean isStringNullOrEmpty(String string) {
		return string == null || string.isEmpty();
	}

	private Response getResponse(String graph, String server) {
		if (this.isStringNullOrEmpty(graph))
			return this.createErrorResponse(Status.BAD_REQUEST, "Must specify the URL of the graph");
		if (this.isStringNullOrEmpty(server))
			return this.createErrorResponse(Status.BAD_REQUEST, "Must specify the URL of the SPARQL point");

		// El controlador retornará el nombre del path
		String nameGraph = controlador.registerGraph(graph, server);

		if (nameGraph.equals("EXISTE"))
			return this.createErrorResponse(Status.CONFLICT, "The graph already exists");

		UriBuilder builder = uriInfo.getAbsolutePathBuilder();
		builder.path(nameGraph);
		return Response.created(builder.build()).build();

	}

	/**
	 * Metodo de consulta para obtener informacion sobre los grafos o sobre un grafo
	 * especifico
	 * 
	 * @param grafo
	 *            Grafo en el que se consulta. Parámetro opcional
	 * 
	 * @param sorted
	 *            Indicamos si la respuesta debe estar ordenada o no
	 * @param limit
	 *            Indicamos cuantos datos se mostrarán
	 * 
	 * @param offset
	 *            Indicamos en que punto empezaremos a mostrar los resultados de la
	 *            consulta
	 * @param format
	 *            Indica si el cliente quiere la información en otro format, por
	 *            defecto es application/atom+xml
	 * 
	 * @return objeto response
	 */
	@GET
	@Produces({ MediaType.APPLICATION_ATOM_XML })
	public Response getGraphMenu(@DefaultValue("") @PathParam("grafo") String grafo,
			@DefaultValue("false") @QueryParam("sorted") boolean sorted,
			@DefaultValue("-1") @QueryParam("limit") int limit, @DefaultValue("0") @QueryParam("offset") int offset,
			@DefaultValue("application/atom+xml") @QueryParam("format") String format) {

		Resource resource = new Resource(grafo, "");
		resource.setSorted(sorted);
		resource.setLimit(limit);
		resource.setOffset(offset);
		resource.setFormat(format);
		return this.getResponse(resource);
	}

	/**
	 * Metodo de consulta para obtener informacion sobre los grafos o sobre un grafo
	 * especifico
	 * 
	 * @param grafo
	 *            Grafo en el que se consulta. Parámetro opcional
	 * 
	 * @param sorted
	 *            Indicamos si la respuesta debe estar ordenada o no
	 * @param limit
	 *            Indicamos cuantos datos se mostrarán
	 * 
	 * @param offset
	 *            Indicamos en que punto empezaremos a mostrar los resultados de la
	 *            consulta
	 * @param format
	 *            Indica si el cliente quiere la información en otro format, por
	 *            defecto es application/atom+xml
	 * 
	 * @return objeto response
	 */
	@GET
	@Path("{graph: .*}")
	public Response getGraphDataMenu(@DefaultValue("") @PathParam("graph") String graph,
			@DefaultValue("false") @QueryParam("sorted") boolean sorted,
			@DefaultValue("-1") @QueryParam("limit") int limit, @DefaultValue("0") @QueryParam("offset") int offset,
			@DefaultValue("application/atom+xml") @QueryParam("format") String format) {

		Resource resource = new Resource(graph, "");
		resource.setSorted(sorted);
		resource.setLimit(limit);
		resource.setOffset(offset);
		resource.setFormat(format);
		return this.getResponse(resource);
	}

	/**
	 * Método de consulta SPARQL para un tipo
	 * 
	 * @param grafo
	 *            Grafo en el que se consulta. Parámetro opcional
	 * 
	 * 
	 * @param clase
	 *            Tipo o clase del grafo. Parámetro obligatorio
	 * 
	 * @param sorted
	 *            Indicamos si la respuesta debe estar ordenada o no
	 * 
	 * @param limit
	 *            Indicamos cuantos datos se mostrarán
	 * 
	 * @param offset
	 *            Indicamos en que punto empezaremos a mostrar los resultados de la
	 *            consulta
	 * @param format
	 *            Indica si el cliente quiere la información en otro format, por
	 *            defecto es text/turtle
	 * @param filter
	 *            Conjunto de condiciones para filtrar el resultado SPARQL
	 * @param sortedBy
	 *            Conjunto de criterios de ordenacion
	 * @return fichero RDF
	 */
	// ((https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]|[a-zA-Z0-9_#-]+)
	@GET
	@Path("{grafo: .* }/classes/{clase :  " + REGULAR_EXPRESSION_INSTANCES_PATH + " }")
	@Produces({ MediaType.APPLICATION_XML, CustomFormats.APPLICATION_TURTLE, MediaType.APPLICATION_JSON,
			MediaType.TEXT_HTML, MediaType.TEXT_PLAIN, CustomFormats.APPLICATION_CSV, CustomFormats.APPLICATION_TSV,
			CustomFormats.TEXT_JAVASCRIPT })
	public Response getInstancesByClass(@DefaultValue("") @PathParam("grafo") String grafo,
			@DefaultValue("") @PathParam("clase") String clase,
			@DefaultValue("false") @QueryParam("sorted") boolean sorted,
			@DefaultValue("false") @QueryParam("distinct") boolean distinct,
			@DefaultValue("-1") @QueryParam("limit") int limit, @DefaultValue("0") @QueryParam("offset") int offset,
			@DefaultValue("text/turtle") @QueryParam("format") String format,
			@DefaultValue("") @QueryParam("filter") String filter,
			@DefaultValue("tipo") @QueryParam("sortedBy") String sortedBy) {

		Resource resource = new Resource(grafo, clase);
		resource.setSorted(sorted);
		resource.setDistinct(distinct);
		resource.setLimit(limit);
		resource.setOffset(offset);
		resource.setFormat(format);
		resource.setFilter(filter);
		resource.setSortedBy(sortedBy);
		return this.getResponse(resource);
	}

	/**
	 * Método de consulta SPARQL para un tipo
	 * 
	 * @param grafo
	 *            Grafo en el que se consulta. Parámetro opcional
	 * 
	 * 
	 * @param clase
	 *            Tipo o clase del grafo. Parámetro obligatorio
	 * 
	 * @param sorted
	 *            Indicamos si la respuesta debe estar ordenada o no
	 * 
	 * @param limit
	 *            Indicamos cuantos datos se mostrarán
	 * 
	 * @param offset
	 *            Indicamos en que punto empezaremos a mostrar los resultados de la
	 *            consulta
	 * @param format
	 *            Indica si el cliente quiere la información en otro format, por
	 *            defecto es text/turtle
	 * @param filter
	 *            Conjunto de condiciones para filtrar el resultado SPARQL
	 * @param sortedBy
	 *            Conjunto de criterios de ordenacion
	 * @return fichero RDF
	 */
	// ((https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]|[a-zA-Z0-9_#-]+)
	@GET
	@Path("{grafo: .* }/classes/count/{clase :  " + REGULAR_EXPRESSION_INSTANCES_PATH + " }")
	@Produces({ MediaType.APPLICATION_XML, CustomFormats.APPLICATION_TURTLE, MediaType.APPLICATION_JSON,
			MediaType.TEXT_HTML, MediaType.TEXT_PLAIN, CustomFormats.APPLICATION_CSV, CustomFormats.APPLICATION_TSV,
			CustomFormats.TEXT_JAVASCRIPT })
	public Response getCountInstancesByClass(@DefaultValue("") @PathParam("grafo") String grafo,
			@DefaultValue("") @PathParam("clase") String clase,
			@DefaultValue("false") @QueryParam("sorted") boolean sorted,
			@DefaultValue("false") @QueryParam("distinct") boolean distinct,
			@DefaultValue("-1") @QueryParam("limit") int limit, @DefaultValue("0") @QueryParam("offset") int offset,
			@DefaultValue("text/turtle") @QueryParam("format") String format,
			@DefaultValue("") @QueryParam("filter") String filter,
			@DefaultValue("tipo") @QueryParam("sortedBy") String sortedBy) {

		Resource resource = new Resource(grafo, clase);
		resource.setSorted(sorted);
		resource.setDistinct(distinct);
		resource.setLimit(limit);
		resource.setOffset(offset);
		resource.setFormat(format);
		resource.setFilter(filter);
		resource.setSortedBy(sortedBy);
		return this.getResponse(resource);
	}

	/**
	 * Método de consulta SPARQL para consultar mediante una propiedad
	 * (ObjectProperty o DatatypeProperty)
	 * 
	 * @param grafo
	 *            Grafo en el que se consulta. Parámetro opcional
	 * 
	 * 
	 * @param propiedad
	 *            Propiedad definida en el grafo. Parámetro obligatorio
	 * @param sorted
	 *            Indicamos si la respuesta debe estar ordenada o no
	 * 
	 * @param limit
	 *            Indicamos cuantos datos se mostrarán
	 * 
	 * @param offset
	 *            Indicamos en que punto empezaremos a mostrar los resultados de la
	 *            consulta
	 * @param format
	 *            Indica si el cliente quiere la información en otro format, por
	 *            defecto es text/turtle
	 * @param filter
	 *            Conjunto de condiciones para filtrar el resultado SPARQL
	 * @param sortedBy
	 *            Conjunto de criterios de ordenacion
	 * @return fichero RDF
	 */
	@GET
	@Path("{grafo : .*}/properties/{propiedad :  " + REGULAR_EXPRESSION_INSTANCES_PATH + " }")
	@Produces({ MediaType.APPLICATION_XML, CustomFormats.APPLICATION_TURTLE, MediaType.APPLICATION_JSON,
			MediaType.TEXT_HTML, MediaType.TEXT_PLAIN, CustomFormats.APPLICATION_CSV, CustomFormats.APPLICATION_TSV,
			CustomFormats.TEXT_JAVASCRIPT })
	public Response getInstancesByProperty(@DefaultValue("") @PathParam("grafo") String grafo,
			@DefaultValue("") @PathParam("propiedad") String propiedad,
			@DefaultValue("") @QueryParam("value") String value,
			@DefaultValue("false") @QueryParam("sorted") boolean sorted,
			@DefaultValue("false") @QueryParam("distinct") boolean distinct,
			@DefaultValue("-1") @QueryParam("limit") int limit, @DefaultValue("0") @QueryParam("offset") int offset,
			@DefaultValue("text/turtle") @QueryParam("format") String format,
			@DefaultValue("") @QueryParam("filter") String filter,
			@DefaultValue("tipo") @QueryParam("sortedBy") String sortedBy) {

		Resource resource = new Resource(grafo, propiedad);
		resource.setValue(value);
		resource.setDistinct(distinct);
		resource.setSorted(sorted);
		resource.setLimit(limit);
		resource.setOffset(offset);
		resource.setFormat(format);
		resource.setFilter(filter);
		resource.setSortedBy(sortedBy);
		return this.getResponse(resource);
	}

	/**
	 * Método de consulta SPARQL para consultar mediante una propiedad
	 * (ObjectProperty o DatatypeProperty)
	 * 
	 * @param grafo
	 *            Grafo en el que se consulta. Parámetro opcional
	 * 
	 * 
	 * @param propiedad
	 *            Propiedad definida en el grafo. Parámetro obligatorio
	 * @param sorted
	 *            Indicamos si la respuesta debe estar ordenada o no
	 * 
	 * @param limit
	 *            Indicamos cuantos datos se mostrarán
	 * 
	 * @param offset
	 *            Indicamos en que punto empezaremos a mostrar los resultados de la
	 *            consulta
	 * @param format
	 *            Indica si el cliente quiere la información en otro format, por
	 *            defecto es text/turtle
	 * @param filter
	 *            Conjunto de condiciones para filtrar el resultado SPARQL
	 * @param sortedBy
	 *            Conjunto de criterios de ordenacion
	 * @return fichero RDF
	 */
	@GET
	@Path("{grafo: [a-zA-Z0-9#_-]*}/properties/count/{propiedad :  " + REGULAR_EXPRESSION_INSTANCES_PATH + " }")
	@Produces({ MediaType.APPLICATION_XML, CustomFormats.APPLICATION_TURTLE, MediaType.APPLICATION_JSON,
			MediaType.TEXT_HTML, MediaType.TEXT_PLAIN, CustomFormats.APPLICATION_CSV, CustomFormats.APPLICATION_TSV,
			CustomFormats.TEXT_JAVASCRIPT })
	public Response getCountInstancesByProperty(@DefaultValue("") @PathParam("grafo") String grafo,
			@DefaultValue("") @PathParam("propiedad") String propiedad,
			@DefaultValue("") @QueryParam("value") String value,
			@DefaultValue("false") @QueryParam("distinct") boolean distinct,
			@DefaultValue("-1") @QueryParam("limit") int limit, @DefaultValue("0") @QueryParam("offset") int offset,
			@DefaultValue("text/turtle") @QueryParam("format") String format,
			@DefaultValue("") @QueryParam("filter") String filter) {

		Resource resource = new Resource(grafo, propiedad);
		resource.setValue(value);
		resource.setDistinct(distinct);
		resource.setLimit(limit);
		resource.setOffset(offset);
		resource.setFormat(format);
		resource.setFilter(filter);
		return this.getResponse(resource);
	}

	/**
	 * Método de consulta SPARQL para consultar mediante una propiedad
	 * (ObjectProperty o DatatypeProperty)
	 * 
	 * @param grafo
	 *            Grafo en el que se consulta. Parámetro opcional
	 * 
	 * 
	 * @param propiedad
	 *            Propiedad definida en el grafo. Parámetro obligatorio
	 * @param sorted
	 *            Indicamos si la respuesta debe estar ordenada o no
	 * 
	 * @param limit
	 *            Indicamos cuantos datos se mostrarán
	 * 
	 * @param offset
	 *            Indicamos en que punto empezaremos a mostrar los resultados de la
	 *            consulta
	 * @param format
	 *            Indica si el cliente quiere la información en otro format, por
	 *            defecto es text/turtle
	 * @param filter
	 *            Conjunto de condiciones para filtrar el resultado SPARQL
	 * @param sortedBy
	 *            Conjunto de criterios de ordenacion
	 * @return fichero RDF
	 */

	private Response getResponse(Resource resource, boolean describe) {

		if (this.isSpecialFormat(resource.getFormat()))
			return this.createSpecialResponse(resource);

		return Response.status(Status.OK).entity(controlador.querySPARQL(OperationType.DESCRIBE_ELEMENT, resource))
				.type(resource.getFormat()).build();
	}

	@GET
	@Path("{grafo: [a-zA-Z0-9#_-]+}/describe/{element :  " + REGULAR_EXPRESSION_DESCRIBE_PATH + " }")
	public Response getDescribeElement(@DefaultValue("") @PathParam("grafo") String grafo,
			@DefaultValue("") @PathParam("element") String element,
			@DefaultValue("text/turtle") @QueryParam("format") String format) {

		Resource resource = new Resource(grafo, element);
		resource.setFormat(format);
		return this.getResponse(resource, true);
	}

	/**
	 * Metodo para registrar un grafo
	 * 
	 * @param ontology
	 *            Ontologia del grafo
	 * @param graph
	 *            URL del grafo
	 * @param server
	 *            URL del servidor donde esta el grafo
	 * @return response
	 */
	@POST
	public Response registerGraph(@DefaultValue("") @FormParam("graph") String graph,
			@DefaultValue("") @FormParam("server") String server) {

		return this.getResponse(graph, server);
	}

	private Response getResponse(String graph) {
		boolean res = controlador.removeGraph(graph);

		return (res ? Response.noContent().build()
				: this.createErrorResponse(Status.NOT_FOUND, "The graph does not exist"));
	}

	/**
	 * Método para eliminar los grafos de alta
	 * 
	 * @param graph
	 *            grafo
	 * @return response
	 */
	@DELETE
	public Response removeGraph(@DefaultValue("") @FormParam("graph") String graph) {
		return this.getResponse(graph);
	}

	// CATALOGOS
	/**
	 * Obtiene el listado de catalogos
	 * 
	 * @param format
	 *            formato del menu
	 * @return objeto response
	 */
	private Response getListCatalogues(String format) {
		String formats = "<formats>" + "<format>application/atom+xml</format>"
				+ "<format>application/hal+json</format>";
		if (!this.isValidFormatEntry(format))
			return this.createErrorResponse(Status.NOT_ACCEPTABLE, "Format not supported." + formats);

		String path = uriInfo.getAbsolutePath().toString();
		if (!path.endsWith("/"))
			path += "/";
		return Response.status(Status.OK).entity(controlador.getListCatalogues(path, format)).type(format).build();

	}

	/**
	 * Obtiene el listado de catalogos
	 * 
	 * @param format
	 *            Formato del punto de entrada. Por defecto ATOM
	 * @return listado de catalogos
	 */
	@GET
	@Path("catalogues")
	public Response getCatalogues(@DefaultValue("application/atom+xml") @QueryParam("format") String format) {
		return this.getListCatalogues(format);
	}

	/**
	 * Registro de catalogos
	 * 
	 * @param uploadedInputStream
	 *            bytes del catalogo
	 * @param fileDetail
	 *            Informacion adicional del catalogo
	 * @return objeto response
	 */
	private Response getResponse(InputStream uploadedInputStream, FormDataContentDisposition fileDetail) {
		String title = controlador.registerCatalogue(uploadedInputStream, fileDetail);
		if (this.isStringNullOrEmpty(title))
			return this.createErrorResponse(Status.BAD_REQUEST, "The catalogue not have title");
		UriBuilder builder = uriInfo.getAbsolutePathBuilder();
		builder.path(title);
		return Response.created(builder.build()).build();
	}

	/**
	 * Registro de catalogo
	 * 
	 * @param uploadedInputStream
	 *            array de bytes del catalogo
	 * @param fileDetail
	 *            Informacion adicional
	 * @return objeto response
	 */
	@POST
	@Path("catalogues")
	@Consumes({ MediaType.MULTIPART_FORM_DATA, CustomFormats.APPLICATION_RDF_XML })
	public Response registerCatalogue(@FormDataParam("catalogue") InputStream uploadedInputStream,
			@FormDataParam("catalogue") FormDataContentDisposition fileDetail) {

		return this.getResponse(uploadedInputStream, fileDetail);
	}

	/**
	 * Obtiene un catalogo
	 * 
	 * @param title
	 *            titulo del catalogo
	 * @param version
	 *            Version del catalogo
	 * @return catalogo
	 */
	private Response getFAIR(String title, String version) {
		Document catalogue = controlador.getCatalogue(title, version, uriInfo.getAbsolutePath().toString());
		if (catalogue == null)
			return this.createErrorResponse(Status.NOT_FOUND, "Catalogue not found");

		return Response.status(Status.OK).entity(catalogue).type(CustomFormats.APPLICATION_RDF_XML).build();
	}

	/**
	 * Obtiene un catalogo
	 * 
	 * @param title
	 *            titulo del catalogo
	 * @return catalogo
	 */
	@GET
	@Path("catalogues/{title : .*}")
	@Produces(CustomFormats.APPLICATION_RDF_XML)
	public Response getCatalogue(@PathParam("title") String title,
			@DefaultValue("") @QueryParam("version") String version) {

		return this.getFAIR(title, version);
	}

	private Response getResponseRemoveCatalogue(String title, String version) {
		boolean res = controlador.removeCatalogue(title, version);

		return (res ? Response.noContent().build()
				: this.createErrorResponse(Status.BAD_REQUEST, "The catalogue dont exists"));
	}

	@DELETE
	@Path("catalogues")
	public Response removeCatalogue(@FormParam("title") String title,
			@DefaultValue("") @FormParam("version") String version) {

		return this.getResponseRemoveCatalogue(title, version);
	}

	/**
	 * Obtiene el listado de formatos disponibles para el describe de la instancia
	 * 
	 * @param instance
	 *            URI de la instancia
	 * @param format
	 *            formato listado
	 * @return
	 */
	private Response getInstanceLinks(String title, String instance, String version, String format) {
		if (!format.equals("application/atom+xml") && !format.equals("application/hal+json"))
			return this.createErrorResponse(Status.NOT_ACCEPTABLE,
					"The only formats can use are: <formats><format>application/atom+xml</format><format>application/hal+json</format></formats>");

		String path = uriInfo.getAbsolutePath().toString();
		if (path.endsWith("/"))
			path.substring(0, path.length() - 1);
		path = path.substring(0, path.lastIndexOf("/"));
		return Response.status(Status.OK)
				.entity(controlador.getListLinks(title, instance, version, path, format, DESCRIBE_VALID_FORMATS))
				.type(format).build();
	}

	/**
	 * Obtiene el listado de formatos para la instancia solicitada
	 * 
	 * @param title
	 *            Titulo del catalogo
	 * @param instance
	 *            URI instancia
	 * @param format
	 *            formato
	 * @return instancia
	 */
	@GET
	@Path("catalogues/{title : .*}/{instance : " + REGULAR_EXPRESSION_DESCRIBE_PATH + "}/info")
	public Response getInstanceDescribeLinks(@PathParam("title") String title, @PathParam("instance") String instance,
			@DefaultValue("application/atom+xml") @QueryParam("format") String format,
			@DefaultValue("") @QueryParam("version") String version) {

		return this.getInstanceLinks(title, instance, version, format);
	}

	/**
	 * Obtiene el describe de la instancia solicitada
	 * 
	 * @param title
	 *            Titulo del catalogo
	 * @param instance
	 *            URI instancia
	 * @param format
	 *            formato
	 * @return instancia
	 */
	private Response getInstanceCatalogue(String title, String instance, String version, String format) {
		if (!this.isValidDescribeFormat(format))
			return this.createErrorResponse(Status.NOT_ACCEPTABLE, "Invalid format" + this.listDescribeFormats());

		if (this.isSpecialFormat(format))
			return Response.status(Status.OK)
					.entity(controlador.getInstanceByCatalogue(title, instance, version, format, true)).type(format)
					.build();

		return Response.status(Status.OK).entity(controlador.getInstanceByCatalogue(title, instance, version, format))
				.type(format).build();
	}

	/**
	 * Obtiene el describe de la instancia solicitada
	 * 
	 * @param title
	 *            Titulo del catalogo
	 * @param instance
	 *            URI instancia
	 * @param format
	 *            formato del describe. Por defecto text/turtle
	 * @return instancia
	 */
	@GET
	@Path("catalogues/{title : .*}/{instance : " + REGULAR_EXPRESSION_DESCRIBE_PATH + "}")
	public Response getInstanceByCatalogue(@PathParam("title") String title, @PathParam("instance") String instance,
			@DefaultValue("text/turtle") @QueryParam("format") String format,
			@DefaultValue("") @QueryParam("version") String version) {

		return this.getInstanceCatalogue(title, instance, version, format);
	}

}
