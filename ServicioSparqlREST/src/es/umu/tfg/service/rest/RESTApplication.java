package es.umu.tfg.service.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.glassfish.jersey.media.multipart.MultiPartFeature;

/**
 * Controlador de aplicaciones de servicios
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
@ApplicationPath("")
public class RESTApplication extends Application {
	private Set<Object> singletons = new HashSet<Object>();
	private Set<Class<?>> empty = new HashSet<Class<?>>();

	public RESTApplication() {
		singletons.add(new ServiceSPARQLRest());
		// Para permitir procesar ficheros
		empty.add(MultiPartFeature.class);
	}

	@Override
	public Set<Class<?>> getClasses() {
		return empty;
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
