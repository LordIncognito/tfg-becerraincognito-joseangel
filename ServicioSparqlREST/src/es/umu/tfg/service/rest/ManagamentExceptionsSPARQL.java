package es.umu.tfg.service.rest;


import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.json.JSONException;

import es.umu.tfg.sparql.config.ConfigException;
import es.umu.tfg.sparql.controller.SparqlException;

/**
 * Controlador de errores del servicio
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
@Provider
public class ManagamentExceptionsSPARQL implements ExceptionMapper<SparqlException> {

	@Override
	public Response toResponse(SparqlException arg0) {
		// Si es un fallo IO entonces es porque no se ha podido acceder al servicio
		// SPARQL o los parametros son incorrectos
		if (arg0.getCause() != null && arg0.getCause() instanceof JSONException) {
			return Response.status(Status.BAD_REQUEST).type(MediaType.APPLICATION_XML)
					.entity("<error>" + arg0.getMessage() + "." + arg0.getCause().getMessage() + "</error>").build();
		} else if (arg0.getCause() != null && arg0.getCause() instanceof ConfigException) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).type(MediaType.APPLICATION_XML)
					.entity("<error>" + arg0.getMessage() + ".\n" + arg0.getCause().getMessage() + "</error>").build();
		}
		return Response.status(Status.INTERNAL_SERVER_ERROR).type(MediaType.APPLICATION_XML)
				.entity("<error>" + arg0.getMessage() + "</error>").build();
	}

}
