package es.umu.tfg.service.formats;

import javax.ws.rs.core.MediaType;

/**
 * Clase que define formatos adicionales no definidos en REST
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
public class CustomFormats {
	// Turtle
	/**
	 * Formato TURTLE
	 */
	public static final String APPLICATION_TURTLE = "text/turtle";
	public static final MediaType TEXT_TURTLE = new MediaType("text", "turtle");
	// Javascript
	/**
	 * Formato JavaScript
	 */
	public static final String TEXT_JAVASCRIPT = "application/javascript";
	public static final MediaType APPLICATION_JAVASCRIPT = new MediaType("application", "javascript");
	// text/csv
	/**
	 * Formato CSV
	 */
	public static final String APPLICATION_CSV = "text/csv";
	public static final MediaType TEXT_CSV = new MediaType("text", "csv");
	// text/tab-separated-values
	/**
	 * Formato TSV
	 */
	public static final String APPLICATION_TSV = "text/tab-separated-values";
	public static final MediaType TEXT_TAB_SEPARATED_VALUES = new MediaType("text", "tab-separated-values");
	/**
	 * Formato RDF/XML
	 */
	public static final String APPLICATION_RDF_XML = "application/rdf+xml";
	public static final MediaType TEXT_RDF_XML = new MediaType("application", "rdf+xml");
	
}
