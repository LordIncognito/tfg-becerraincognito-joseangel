package es.umu.tfg.sparql.config;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Clase con metodos de apoyo general
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
public class Utils {

	private Utils() {

	}

	/**
	 * Método para codificar una cadena a una codificación especifica
	 * 
	 * @param cadena
	 *            Cadena a codificar
	 * @param codificacion
	 *            Tipo de codificacion
	 * @return cadena codificada
	 * @throws ConfigException
	 */
	public static String cadenaURLEncoder(String cadena, String codificacion) throws ConfigException {
		String cadenaCodificada = "";
		if (cadena == null)
			return cadenaCodificada;

		try {
			cadenaCodificada = URLEncoder.encode(cadena, codificacion);
		} catch (UnsupportedEncodingException e) {
			throw new ConfigException("Fail to codify the string", e);
		}
		return cadenaCodificada;
	}

	/**
	 * Metodo para crear un objeto XPATH con la expresion XPATH especificada
	 * 
	 * @param expression
	 *            expresion XPATH
	 * @return objeto XPATH
	 * @throws ConfigException
	 */
	private static XPathExpression createXPath(String expression) throws ConfigException {
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		xpath.setNamespaceContext(new NameSpaces());
		XPathExpression expr = null;
		try {
			expr = xpath.compile(expression);
		} catch (XPathExpressionException e) {
			throw new ConfigException("Invalid expression");
		}
		return expr;
	}

	/**
	 * Método para cargar un documento en memoria
	 * 
	 * @param urlDoc
	 *            URL o nombre del documento
	 * @return documento
	 * @throws ConfigException
	 */
	public static Document loadDocument(String urlDoc) throws ConfigException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document doc = null;
		try {
			factory.setNamespaceAware(true);
			factory.setValidating(false);
			factory.setFeature("http://xml.org/sax/features/namespaces", false);
			factory.setFeature("http://xml.org/sax/features/validation", false);
			factory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			builder = factory.newDocumentBuilder();
			if (urlDoc.startsWith("http"))
				doc = builder.parse(urlDoc);
			else
				doc = builder.parse(new File(urlDoc).toURI().toString());
		} catch (ParserConfigurationException e) {
			throw new ConfigException("Error to read file");
		} catch (SAXException e) {
			throw new ConfigException("Syntax error by SAX");
		} catch (IOException e) {
			return null;
		}
		return doc;
	}

	/**
	 * Guarda un documento XML en el path especificado
	 * 
	 * @param doc
	 *            documento
	 * @param path
	 *            ubicación de guardado
	 * @throws ConfigException
	 */
	public static void saveDocument(Document doc, String path) throws ConfigException {
		// 1. Construye la factoría de transformación y obtiene un
		// transformador
		TransformerFactory tFactoria = TransformerFactory.newInstance();
		Transformer transformacion = null;
		// 2. Establece la entrada, como un árbol DOM
		Source input = new DOMSource(doc);
		// 3. Establece la salida, un fichero en disco
		Result output = new StreamResult(path);
		try {
			transformacion = tFactoria.newTransformer();
			// 4. Aplica la transformación
			transformacion.transform(input, output);
		} catch (TransformerConfigurationException e) {
			throw new ConfigException("Transformer configuration invalid");
		} catch (TransformerException e) {
			throw new ConfigException("Fail to save doc");

		}

	}

	/**
	 * Metodo para obtener un Nodo del documento
	 * 
	 * @param doc
	 *            documento
	 * @param expresion
	 *            expresion XPATH
	 * @return nodo
	 * @throws ConfigException
	 */
	public static Node getNode(Document doc, String expresion) throws ConfigException {
		XPathExpression expr = createXPath(expresion);
		Node nodo = null;
		try {
			nodo = (Node) expr.evaluate(doc, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new ConfigException("Invalid XPATH expression");
		}

		return nodo;
	}

	/**
	 * Metodo para obtener una lista de nodos del documento
	 * 
	 * @param doc
	 *            documento
	 * @param expresion
	 *            expresion XPATH
	 * @return lista de nodos
	 * @throws ConfigException
	 */
	public static NodeList getNodes(Document doc, String expresion) throws ConfigException {
		XPathExpression expr = createXPath(expresion);
		NodeList lista = null;
		try {
			lista = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			throw new ConfigException("Invalid XPATH expression");
		}

		return lista;
	}

	/**
	 * Método para transformar la fecha a String
	 * 
	 * @param fecha
	 *            fecha
	 * @return fecha string
	 */
	private static String formatFecha(Date fecha) {
		DateFormat formato = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

		return formato.format(fecha);

	}

	/**
	 * Método para pasar de tipo Date a XMLGregorianCalendar
	 * 
	 * @param fecha
	 *            Fecha
	 * @return fecha en formato XML
	 * @throws ConfigException
	 */
	public static XMLGregorianCalendar createFechaXML(Date fecha) throws ConfigException {

		XMLGregorianCalendar fechaXML = null;

		try {
			fechaXML = DatatypeFactory.newInstance().newXMLGregorianCalendar(formatFecha(fecha));
		} catch (Exception e) {
			throw new ConfigException("Error para crear la fechaXML", e);
		}

		return fechaXML;
	}

	/**
	 * Obtiene la subcadena a partir del limitador especificado
	 * 
	 * @param input
	 *            cadena
	 * @param limiter
	 *            limitador
	 * @return subcadena
	 */
	public static String splitStringByChar(String input, String limiter) {
		if (input.lastIndexOf(limiter) < 0)
			return input;

		return input.substring(input.lastIndexOf(limiter) + 1, input.length());
	}

}
