package es.umu.tfg.sparql.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.glassfish.hk2.utilities.general.IndentingXMLStreamWriter;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import es.umu.tfg.sparql.parameters.Resource;
import es.umu.tfg.sparql.queryBuilder.OperationType;
import es.umu.tfg.sparql.queryBuilder.QueryBuilder;

/**
 * Controlador para generar los ficheros de configuracion del servicioSPARQL
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
public class ControllerSPARQLConfig {
	private static ControllerSPARQLConfig uniqueInstance;
	private static final String DIR_CONFIG = "config";
	private static final String DIR_CATALOGUES = "catalogues";
	/**
	 * Código estatico para comprobar que directorio donde se guardar la
	 * configuración esta y el de los catalogos
	 */
	static {
		File dir = new File(DIR_CONFIG);
		if (!dir.exists())
			dir.mkdir();
		dir = new File(DIR_CATALOGUES);
		if (!dir.exists())
			dir.mkdir();

	}

	private ControllerSPARQLConfig() {
	}

	public static ControllerSPARQLConfig getUniqueInstance() {
		if (uniqueInstance == null)
			uniqueInstance = new ControllerSPARQLConfig();
		return uniqueInstance;
	}

	/**
	 * Crear un stream de escritura
	 * 
	 * @param file
	 *            nombre del grafo
	 * @return stream
	 * @throws ConfigException
	 */
	private FileOutputStream createStream(String file) throws ConfigException {
		FileOutputStream stream = null;
		try {
			if (file.contains("."))
				file = file.replaceAll("\\.", "_");
			stream = new FileOutputStream(DIR_CONFIG + "/" + file + ".xml");
		} catch (FileNotFoundException e) {
			throw new ConfigException("Fail to create stream ");
		}
		return stream;
	}

	/**
	 * Cerrar el stream
	 * 
	 * @param stream
	 * @throws ConfigException
	 */
	private void closeStream(FileOutputStream stream) throws ConfigException {
		try {
			stream.flush();
			stream.close();
		} catch (IOException e) {
			throw new ConfigException("Fail to close stream");
		}
	}

	/**
	 * Metodo para crear un manejador StAX para escribir el fichero de configuracion
	 * 
	 * @return manejador StAX
	 * @throws ConfigException
	 */
	private XMLStreamWriter createWriter(FileOutputStream stream) throws ConfigException {
		XMLStreamWriter writer = null;
		XMLOutputFactory xof = XMLOutputFactory.newInstance();
		try {
			writer = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(stream, "UTF-8"));
		} catch (XMLStreamException e) {
			throw new ConfigException("Error StAX");
		}
		return writer;
	}

	/**
	 * Metodo para escribir un elemento especifico en el fichero de configuracion
	 * 
	 * @param writer
	 *            Manejador StAX
	 * @param tag
	 *            Nombre de la etiqueta
	 * @param value
	 *            Valor de la etiqueta
	 * @throws ConfigException
	 */
	private void writeElement(XMLStreamWriter writer, String tag, String value) throws ConfigException {
		try {
			writer.writeStartElement(tag);
			writer.writeCharacters(value);
			writer.writeEndElement();
		} catch (XMLStreamException e) {
			throw new ConfigException("Fallo en escribir en el fichero de configuracion");
		}

	}

	/**
	 * Metodo para crear el prologo del fichero de configuracion
	 * 
	 * @param writer
	 *            Manejador StAX
	 * @throws ConfigException
	 */
	private void startDoc(XMLStreamWriter writer) throws ConfigException {
		try {
			// Inicio documento
			writer.writeStartDocument("UTF-8", "1.0");
			// A�adimos el nodo padre principal
			writer.writeStartElement("server");
		} catch (XMLStreamException e) {
			throw new ConfigException("Fail to start the document");
		}
	}

	/**
	 * Metodo para cerrar el fichero de configuracion
	 * 
	 * @param writer
	 *            Manejador StAX
	 * @throws ConfigException
	 */
	private void closeDoc(XMLStreamWriter writer) throws ConfigException {
		try {
			// Fin de recursos
			writer.writeEndElement();
			// Fin del documento
			writer.writeEndDocument();
			// Limpieza del buff y cierre del descriptor de fichero
			writer.flush();
			writer.close();
		} catch (XMLStreamException e) {
			throw new ConfigException("Error to close file");
		}
	}

	/**
	 * Métod para cargar el fichero de configuración del grafo
	 * 
	 * @param nameFile
	 *            Nombre de fichero
	 * 
	 * @return fichero de configuracion
	 * @throws ConfigException
	 */
	private Document loadConfigFile(String nameFile) throws ConfigException {
		String nameFileEncode = "";
		try {
			nameFileEncode = URLEncoder.encode(nameFile, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new ConfigException("Error to codify the name of the file", e);
		}
		Document configuracion = Utils.loadDocument(DIR_CONFIG + "/" + nameFileEncode + ".xml");
		return configuracion;
	}

	/**
	 * Método para comprobar si el grafo existe
	 * 
	 * @param nameServer
	 *            Nombre de fichero
	 * @param graph
	 *            URL del grafo
	 * @return true/false
	 * @throws ConfigException
	 */
	public boolean isGraphRegistered(String nameServer, String graph) throws ConfigException {

		Document file = this.loadConfigFile(nameServer);
		if (file == null)
			return false;
		NodeList graphs = file.getElementsByTagName("graph");
		int length = graphs.getLength();
		Node graphNode = null;
		for (int i = 0; i < length; i++) {
			graphNode = graphs.item(i);
			if (graphNode.getTextContent().equals(graph))
				return true;
		}

		return false;
	}

	/**
	 * Metodo para comprobar que se tiene acceso a ese grafo
	 * 
	 * @param nameServer
	 *            Nombre del server
	 * @param graph
	 *            Nombre del grafo
	 * @return true/false
	 * @throws ConfigException
	 */
	public boolean isValidQuery(String nameServer, String graph) throws ConfigException {
		Document ficheroConfiguracion = this.loadConfigFile(nameServer);
		String uriGraph = this.getGrafoURI(ficheroConfiguracion, graph);
		return !uriGraph.isEmpty();
	}

	/**
	 * Método para obtener la URL del grafo en el fichero de configuración
	 * 
	 * @param grafo
	 *            Nombre del grafo
	 * @return URL del grafo
	 * @throws ConfigException
	 */
	public String getGrafoURI(String nameServer, String grafo) throws ConfigException {
		Document configuracion = this.loadConfigFile(nameServer);
		Node uri = Utils.getNode(configuracion, "/server/graph[contains(text(), \"" + grafo + "\")]");
		return (uri != null ? uri.getTextContent() : "");
	}

	/**
	 * Obtiene la URL del grafo
	 * 
	 * @param file
	 *            Fichero de configuracion
	 * @param grafo
	 *            nombre del grafo
	 * @return URL del grafo
	 * @throws ConfigException
	 */
	public String getGrafoURI(Document file, String grafo) throws ConfigException {
		Node uri = Utils.getNode(file, "/server/graph[contains(text(), '" + grafo + "')]");
		return (uri != null ? uri.getTextContent() : "");
	}

	/**
	 * Obtiene el fichero de configuracion
	 * 
	 * @param graph
	 *            grafo
	 * @return fichero de configuracion
	 * @throws ConfigException
	 */
	private Document getFileConfig(String graph) throws ConfigException {
		File dir = new File(DIR_CONFIG);
		File[] configFiles = dir.listFiles();
		Document file = null;
		for (File configFile : configFiles) {
			String pathFile = configFile.getAbsolutePath();
			file = Utils.loadDocument(pathFile);
			String uriGraph = this.getGrafoURI(file, graph);
			if (uriGraph != null && !uriGraph.isEmpty())
				return file;
		}

		return null;
	}

	/**
	 * Obtiene el PATH donde se almacena el fichero de configuracion que contiene la
	 * URL del grafo
	 * 
	 * @param graph
	 *            grafo
	 * @return path
	 * @throws ConfigException
	 */
	private String getPathFileConfig(String graph) throws ConfigException {
		File dir = new File(DIR_CONFIG);
		File[] configFiles = dir.listFiles();
		Document file = null;
		for (File configFile : configFiles) {
			String pathFile = configFile.getAbsolutePath();
			file = Utils.loadDocument(pathFile);
			String uriGraph = this.getGrafoURI(file, graph);
			if (uriGraph != null && !uriGraph.isEmpty())
				return pathFile;
		}

		return "";
	}

	/**
	 * Obtiene la URL del servidor donde se almacena el grafo
	 * 
	 * @param graph
	 *            grafo
	 * @return URL del servidor
	 *
	 * @throws ConfigException
	 */
	public String getServerPoint(String graph) throws ConfigException {
		Document configuracion = this.getFileConfig(graph);
		Node nodo = Utils.getNode(configuracion, "/server/url-server");
		return (nodo != null ? nodo.getTextContent() : "");
	}

	/**
	 * Comprueba si el fichero del servidor ya fue registrado
	 * 
	 * @param nameServer
	 *            Nombre del servidor
	 * @return true/false
	 * @throws ConfigException
	 */
	public boolean existsFile(String nameServer) throws ConfigException {
		String nameServerEncode = Utils.cadenaURLEncoder(nameServer, "UTF-8");
		return new File(DIR_CONFIG + "/" + nameServerEncode + ".xml").exists();
	}

	/**
	 * Actualiza el fichero de configuracion del servidor. Le añade el nuevo grafo
	 * 
	 * @param nameServer
	 *            Nombre del servidor
	 * @param graph
	 *            URL del grafo nuevo
	 * @return nombre del grafo
	 * @throws ConfigException
	 */
	private String updateFile(String nameServer, String graph) throws ConfigException {
		Document file = this.loadConfigFile(nameServer);
		Element newGraphNode = file.createElement("graph");
		newGraphNode.setTextContent(graph);
		NodeList rootList = file.getElementsByTagName("server");
		Node root = rootList.item(0);
		NodeList graphs = file.getElementsByTagName("graph");
		Node lastGraphRegistered = graphs.item(graphs.getLength() - 1);
		root.insertBefore(newGraphNode, lastGraphRegistered);
		Utils.saveDocument(file, DIR_CONFIG + "/" + nameServer + ".xml");
		String nameGraph = Utils.splitStringByChar(graph, "/");
		if (nameGraph.matches("\\..*")) {
			nameGraph = nameGraph.replaceAll("\\..*", "");
		}
		return nameGraph;
	}

	/**
	 * Registra el grafo en el sistema
	 * 
	 * @param nameGraph
	 *            Nombre del grafo (el que se usará en el PATH)
	 * @param ontology
	 *            Ontologia del grafo
	 * @param graph
	 *            URL del grafo
	 * @param access
	 *            Servidor donde esta el grafo
	 * @return
	 * @throws ConfigException
	 */
	public String registerGraph(String nameServer, String graph, String server) throws ConfigException {

		if (this.existsFile(nameServer))
			return this.updateFile(nameServer, graph);

		FileOutputStream stream = this.createStream(nameServer);
		// Creamos el fichero
		XMLStreamWriter writer = this.createWriter(stream);
		this.startDoc(writer);
		// Escribimos la URI del grafo
		this.writeElement(writer, "graph", graph);
		// Escribimos la URI del punto de acceso
		this.writeElement(writer, "url-server", server);
		this.closeDoc(writer);
		this.closeStream(stream);
		String nameGraph = Utils.splitStringByChar(graph, "/");
		if (nameGraph.matches("\\..*")) {
			nameGraph = nameGraph.replaceAll("\\..*", "");
		}
		return nameGraph;
	}

	private List<String> getGraphsNode(Document configFile) throws ConfigException {
		List<String> graphs = new LinkedList<String>();
		NodeList nodeGraphs = Utils.getNodes(configFile, "/server//graph");
		int length = nodeGraphs.getLength();
		for (int i = 0; i < length; i++) {
			graphs.add(nodeGraphs.item(i).getTextContent());
		}
		return graphs;
	}

	/**
	 * Método para obtener el listado de grafos que estan registrados en el sistema
	 * 
	 * @return grafos
	 * @throws ConfigException
	 */
	public List<String> getGrafos() throws ConfigException {
		File directory = new File(DIR_CONFIG);
		File[] graphsFiles = directory.listFiles();
		List<String> graphs = new LinkedList<String>();
		Document configFile = null;
		for (File graphFile : graphsFiles) {
			String pathFile = graphFile.getAbsolutePath();
			configFile = Utils.loadDocument(pathFile);
			graphs.addAll(this.getGraphsNode(configFile));
		}

		return graphs;
	}

	/**
	 * Elimina el grafo del sistema
	 * 
	 * @param graph
	 *            nombre del grafo
	 * @return true/false
	 * @throws ConfigException
	 */
	public boolean removeGraph(String graph) throws ConfigException {
		Document file = this.getFileConfig(graph);
		if (file == null)
			return false;
		Node graphNode = Utils.getNode(file, "/server/graph[contains(text(), '" + graph + "')]");
		if (graphNode == null)
			return false;
		file.getDocumentElement().removeChild(graphNode);
		NodeList graphs = Utils.getNodes(file, "/server/*[local-name() = 'graph']");
		String pathFichero = this.getPathFileConfig(graph);
		Utils.saveDocument(file, pathFichero);
		// Si ya no se tiene acceso a grafos de dicho server pues se elimina dicho
		// fichero
		if (graphs == null || graphs.getLength() == 0) {
			File fichero = new File(pathFichero);
			if (fichero.exists())
				fichero.delete();
		}
		return true;
	}

	// Catalogos
	/**
	 * Obtiene el listado de catalogos registrados en la API
	 * 
	 * @return titulos de catalogos
	 * @throws ConfigException
	 */
	public List<String> getListCatalogues() throws ConfigException {
		List<String> titles = new LinkedList<String>();
		File dirCatalogues = new File(DIR_CATALOGUES);
		File[] catalogues = dirCatalogues.listFiles();
		for (File catalogue : catalogues) {
			String name = catalogue.getName();
			name = name.substring(0, name.lastIndexOf("."));
			if (!titles.contains(name))
				titles.add(name);
		}
		return titles;
	}

	/**
	 * Crear un fichero temporal
	 * 
	 * @param nameFile
	 *            Nombre del fichero
	 * @param sufix
	 *            extension del fichero
	 * @return fichero
	 * @throws ConfigException
	 */
	private File createTmpFile(String nameFile, String sufix) throws ConfigException {
		File tmpFile = null;
		try {
			tmpFile = File.createTempFile(nameFile, sufix);
		} catch (IOException e) {
			throw new ConfigException("Fail to create tmp file");
		}
		return tmpFile;
	}

	/**
	 * Escritura de fichero
	 * 
	 * @param tmpFile
	 *            Fichero de entrada
	 * @param rdf
	 *            bytes
	 * @throws ConfigException
	 */
	private void writeTmpFile(File tmpFile, InputStream rdf) throws ConfigException {
		// Tamaño del buffer
		int buffSize = 4096;
		byte[] buff = new byte[buffSize];
		int read = 0;
		FileOutputStream writer = null;
		try {
			// Creamos el fichero y su respectivo descriptor
			writer = new FileOutputStream(tmpFile);
			// Escribimos
			while ((read = rdf.read(buff)) != -1) {
				writer.write(buff, 0, read);
			}
			writer.flush();
			writer.close();
		} catch (FileNotFoundException e) {
			throw new ConfigException("Tmp file not found");
		} catch (IOException e) {
			throw new ConfigException("Error to write in tmp file");
		}

	}

	/**
	 * Retorna el tipo de la URI del concepto que se trata. Si es una clase o una
	 * propiedad o relacion
	 * 
	 * @param builder
	 *            Constructor de sentencias SPARQL
	 * @param uri
	 *            URI del concepto
	 * @param graph
	 *            URL del Grafo
	 * @param urlServer
	 *            URL del servidor
	 * @return tipo de la URI
	 * @throws ConfigException
	 */
	private String getUriType(QueryBuilder builder, String uri, String graph, String urlServer) throws ConfigException {
		Resource resource = new Resource();
		resource.setUrl(uri);
		String query = builder.getQuery(OperationType.DESCRIBE_ELEMENT, resource);
		String url = urlServer + "?" + "default-graph-uri=" + Utils.cadenaURLEncoder(graph, "UTF-8") + "&query="
				+ Utils.cadenaURLEncoder(query, "UTF-8") + "&format="
				+ Utils.cadenaURLEncoder("application/rdf+xml", "UTF-8");

		Document describe = Utils.loadDocument(url);
		Node typeUri = Utils.getNode(describe, "/RDF/Description/type/@resource");
		return (typeUri != null ? typeUri.getTextContent() : "");
	}

	/**
	 * Obtiene el prefijo usado para el esquema de instancias
	 * 
	 * @param description
	 *            Nodo principal del catalogo
	 * @return prefijo
	 */
	private String getPrefixInstance(Node description) {
		String urlInstances = "http://www.w3.org/ns/ldp#";
		NamedNodeMap attrs = description.getAttributes();
		int length = attrs.getLength();
		for (int i = 0; i < length; i++) {
			Node attr = attrs.item(i);
			if (attr.getTextContent().equals(urlInstances))
				return Utils.splitStringByChar(attr.getNodeName(), ":");
		}
		return "";
	}

	/**
	 * Rellena los campos del catalogo dedicados para las instancias
	 * 
	 * @param catalogue
	 *            Catalogo
	 * @param docInstances
	 *            Instancias
	 * @param path
	 *            Path de acceso
	 * @throws ConfigException
	 */
	private void setInstances(Document catalogue, Document docInstances, String path, String version)
			throws ConfigException {
		NodeList listInstances = docInstances.getElementsByTagName("uri");
		Node description = Utils.getNode(catalogue, "/RDF/Description");
		Node entities = Utils.getNode(catalogue, "/RDF/Description/*[local-name() = 'entities']");
		if (entities == null) {
			entities = catalogue.createElement("entities");
			description.appendChild(entities);
		}
		int length = listInstances.getLength();
		entities.setTextContent(Integer.toString(length));
		String prefix = this.getPrefixInstance(description);
		String queryParam = (version.isEmpty() ? "" : "?version=" + version);
		for (int i = 0; i < length; i++) {
			Node instance = catalogue.createElement(prefix + ":contains");
			String uriInstance = listInstances.item(i).getTextContent().replaceAll("#",
					Utils.cadenaURLEncoder("#", "UTF-8"));
			// Ponemos la URL del servicio + URI de la instancia
			((Element) instance).setAttribute("rdf:resource", path + uriInstance + "/info" + queryParam);
			description.appendChild(instance);
		}
	}

	/**
	 * Obtiene la sentencia SPARQL contenida en la descripcion del catalogo
	 * 
	 * @param catalogue
	 *            Catalogo
	 * @return sentencia SPARQL
	 * @throws ConfigException
	 */
	private String getSparqlQuery(Document catalogue) throws ConfigException {
		Node descriptionSPARPQL = Utils.getNode(catalogue, "/RDF/Description/*[local-name() = 'description']");
		if (descriptionSPARPQL == null)
			return "";

		// Obtenemos la descripcion
		String text = descriptionSPARPQL.getTextContent();
		// ER para obtener los prefijos (si hay)
		String prefixRegex = "((PREFIX|prefix) \\w+:<(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]+>\\s*)+";
		// ER para obtener el select
		String selectBody = "(SELECT|select)\\s*(DISTINCT|distinct)?\\s*(\\?.+)+\\{.+\\}";
		String selectOrderBy = "((ORDER BY|order by)(\\s+)(((ASC|asc)|(DESC|desc))?\\((\\?\\w+)\\)(,\\s+|\\s+)|(\\?\\w+)(,\\s+|\\s+)))";
		String selectRegex = selectBody + "(\\s)+?" + selectOrderBy + "?(\\s*)((LIMIT|limit) [0-9]+)?(\\s*)((OFFSET|offset) [0-9]+)?(\\s|$)";
		String prefix = "";
		String select = "";
		// Buscamos los prefijos
		Pattern pattern = Pattern.compile(prefixRegex, Pattern.DOTALL);
		Matcher m = pattern.matcher(text);
		if (m.find())
			prefix = m.group() + "\n";
		// Buscamos la sentencia SPARQL
		pattern = Pattern.compile(selectRegex, Pattern.DOTALL);
		m = pattern.matcher(text);
		if (m.find())
			select = m.group();
		// Retornamos la combinacion de los prefijos con la sentencia
		return prefix + select;
	}

	/**
	 * Obtiene las instancias del catalogo y las inserta en el documento
	 * 
	 * @param catalogue
	 *            Catalogo
	 * @param server
	 *            URL del servidor
	 * @param graph
	 *            URL del grafo
	 * @param query
	 *            Sentencia SPARQL
	 * @param path
	 *            Path de acceso
	 * @throws ConfigException
	 */
	private void setDataByOwnQuery(Document catalogue, String server, String graph, String query, String path,
			String version) throws ConfigException {
		String url = server + "?" + "default-graph-uri=" + Utils.cadenaURLEncoder(graph, "UTF-8") + "&query="
				+ Utils.cadenaURLEncoder(query, "UTF-8") + "&format="
				+ Utils.cadenaURLEncoder("application/xml", "UTF-8");

		Document docInstances = Utils.loadDocument(url);
		this.setInstances(catalogue, docInstances, path, version);
	}

	
	private void fillFields(Document catalogue) throws ConfigException {
		Node description = Utils.getNode(catalogue, "/RDF/Description");
		Node entities = Utils.getNode(catalogue, "/RDF/Description/*[local-name() = 'entities']");
		if (entities == null) {
			entities = catalogue.createElement("entities");
			description.appendChild(entities);
		}
		entities.setTextContent("0");
	}
	/**
	 * Rellena los campos dinamicos del catalogo: cantidad de instancias, listado de
	 * instancias, etc.
	 * 
	 * @param catalogue
	 *            Catalogo
	 * @param path
	 *            Path de acceso
	 * @throws ConfigException
	 */
	private void setData(Document catalogue, String path, String version) throws ConfigException {
		NodeList nodesPage = Utils.getNodes(catalogue, "/RDF/Description//*[local-name() = 'page']/@resource");
		String sparqlServer = nodesPage.item(0).getTextContent();
		String uriGraph = nodesPage.item(1).getTextContent();
		String sparqlQuery = this.getSparqlQuery(catalogue);
		System.out.println("SPARQL QUERY DEL CATALOGO:");
		System.out.println(sparqlQuery);
		System.out.println("------------------");
		if (!sparqlQuery.isEmpty())
			this.setDataByOwnQuery(catalogue, sparqlServer, uriGraph, sparqlQuery, path, version);
		else {
			Node uriElement = Utils.getNode(catalogue, "/RDF/Description/@about");
			if (uriElement == null)
				throw new ConfigException("The catalogue must have a rdf:resource in the description");
			String uri = uriElement.getTextContent();
			// ¿Como saber si es una URI de clase o de propiedad?
			QueryBuilder builder = new QueryBuilder();
			String query = "";
			Resource resource = new Resource();
			resource.setUrl(uri);
			resource.setSorted(true);
			String typeUri = this.getUriType(builder, uri, uriGraph, sparqlServer);
			Node description = Utils.getNode(catalogue, "/RDF/Description");
			Node descriptionSPARQL = Utils.getNode(catalogue, "/RDF/Description/*[local-name() = 'description']");
			if (descriptionSPARQL == null) {
				descriptionSPARQL = catalogue.createElement("description");
				description.appendChild(descriptionSPARQL);
			}
			String textDescripcion = descriptionSPARQL.getTextContent();
			//Si no hay un tipo entonces no se hace nada
			if (typeUri.isEmpty()) {
				this.fillFields(catalogue);
			}
			else if (typeUri.equals("http://www.w3.org/2002/07/owl#Class")) {
				query = builder.getQuery(OperationType.GET_ELEMENTS_BY_CLASS, resource);
				String url = sparqlServer + "?" + "default-graph-uri=" + Utils.cadenaURLEncoder(uriGraph, "UTF-8")
						+ "&query=" + Utils.cadenaURLEncoder(query, "UTF-8") + "&format="
						+ Utils.cadenaURLEncoder("application/xml", "UTF-8");

				
				descriptionSPARQL.setTextContent(textDescripcion + "\nThe SPARQL query is: " + query);
				Document docInstances = Utils.loadDocument(url);
				this.setInstances(catalogue, docInstances, path, version);
			} else {
				query = builder.getQuery(OperationType.GET_ELEMENTS_BY_PROPERTY, resource);
				String url = sparqlServer + "?" + "default-graph-uri=" + Utils.cadenaURLEncoder(uriGraph, "UTF-8")
						+ "&query=" + Utils.cadenaURLEncoder(query, "UTF-8") + "&format="
						+ Utils.cadenaURLEncoder("application/xml", "UTF-8");

				
				descriptionSPARQL.setTextContent(textDescripcion + "\nThe SPARQL query is: " + query);
				Document docInstances = Utils.loadDocument(url);
				this.setInstances(catalogue, docInstances, path, version);

			}
		}
	}

	private boolean isNodesURLsEmpty(NodeList urls) {
		return urls.item(0).getTextContent().isEmpty() || urls.item(1).getTextContent().isEmpty();
	}

	/**
	 * Registra un catalogo
	 * 
	 * @param rdf
	 *            bytes del catalogo
	 * @param fileDetail
	 *            Informacion adicional del catalogo
	 * @return titulo del catalogo
	 * @throws ConfigException
	 */
	public String registerCatalogue(InputStream rdf, FormDataContentDisposition fileDetail) throws ConfigException {
		String nameTmpFile = (fileDetail.getFileName() != null ? fileDetail.getFileName() : "new_catalogue");
		File tmpFile = this.createTmpFile(nameTmpFile, "rdf");
		this.writeTmpFile(tmpFile, rdf);
		Document docFile = Utils.loadDocument(tmpFile.getAbsolutePath());
		Node root = Utils.getNode(docFile, "/RDF");
		if (tmpFile.exists())
			tmpFile.delete();
		if (root == null)
			throw new ConfigException("The catalogue must be RDF");

		Node nodeAbout = Utils.getNode(docFile, "/RDF/Description/@about");
		if (nodeAbout == null || nodeAbout.getTextContent().isEmpty())
			throw new ConfigException(
					"The catalogue must have URL about the theme (<rdf:Description rdf:about=...> </rdf:Description>");
		Node nodeVersion = Utils.getNode(docFile, "/RDF/Description/*[local-name() = 'version']");
		if (nodeVersion == null || nodeVersion.getTextContent().isEmpty())
			throw new ConfigException("The catalogue must have version (<version>...</version>)");
		NodeList nodesPage = Utils.getNodes(docFile, "/RDF/Description//*[local-name() = 'page']/@resource");
		if (nodesPage == null || nodesPage.getLength() < 2 || this.isNodesURLsEmpty(nodesPage))
			throw new ConfigException(
					"The catalogue must have the pages of the resource <page>URL of SPARQL point</page><page>URI of the graph</page>");

		Node title = Utils.getNode(docFile, "/RDF/Description/*[local-name() = 'title']");
		if (title == null || title.getTextContent().isEmpty())
			return "";

		String titleAccess = title.getTextContent().replaceAll("\\s", "_");
		Utils.saveDocument(docFile, DIR_CATALOGUES + "/" + titleAccess + "."
				+ Utils.cadenaURLEncoder(nodeVersion.getTextContent(), "UTF-8") + ".rdf");
		return titleAccess;
	}

	/*
	 * private Document selectCatalogue(List<Document> cataloguesSelected, String
	 * version) throws ConfigException { if (cataloguesSelected.isEmpty()) return
	 * null;
	 * 
	 * String lastVersionSelected = ""; Node nodeVersion = null; Document
	 * catalogueSelected = null; String versionCatalogue = ""; for (Document
	 * catalogue : cataloguesSelected) { nodeVersion = Utils.getNode(catalogue,
	 * "/RDF/Description/*[local-name() = 'version']"); versionCatalogue =
	 * nodeVersion.getTextContent(); // Si se esta buscando una version especifica
	 * pues la retornamos al encontrarla if (!version.isEmpty() &&
	 * versionCatalogue.equals(version)) return catalogue; // Si no se esta buscando
	 * una version especifica, buscaremos la ultima y la // retornamos else if
	 * (lastVersionSelected.isEmpty()) { lastVersionSelected = versionCatalogue;
	 * catalogueSelected = catalogue; } else if
	 * (lastVersionSelected.compareTo(versionCatalogue) < 0) { lastVersionSelected =
	 * versionCatalogue; catalogueSelected = catalogue; } }
	 * 
	 * return catalogueSelected; }
	 */

	private String getVersionCatalogue(String nameCatalogue) {
		int posPunto = nameCatalogue.lastIndexOf(".");
		int posVersion = nameCatalogue.lastIndexOf(".", posPunto - 1) + 1;

		return nameCatalogue.substring(posVersion, posPunto);
	}

	private String getTitleCatalogue(String nameCatalogue) {
		int posPuntoVersion = nameCatalogue.indexOf(".");
		return nameCatalogue.substring(0, posPuntoVersion);
	}

	/**
	 * Busca el catalogo en el directorio donde se almacenan
	 * 
	 * @param title
	 *            Titulo del catalogo
	 * @param version
	 *            Version del catalogo
	 * @return catalogo
	 * @throws ConfigException
	 */
	private Document searchCatalogue(String title, String version) throws ConfigException {
		File dirCatalogues = new File(DIR_CATALOGUES);
		File[] catalogues = dirCatalogues.listFiles();
		String nameCatalogue = "";
		String versionCatalogue = "";
		String titleCatalogue = "";
		String lastVersionSelected = "";
		File catalogueSelected = null;
		for (File catalogue : catalogues) {
			nameCatalogue = catalogue.getName();
			versionCatalogue = this.getVersionCatalogue(nameCatalogue);
			titleCatalogue = this.getTitleCatalogue(nameCatalogue);
			if (!version.isEmpty() && titleCatalogue.equals(title) && version.equals(versionCatalogue))
				return Utils.loadDocument(catalogue.getAbsolutePath());
			else if (titleCatalogue.equals(title) && lastVersionSelected.isEmpty()) {
				lastVersionSelected = versionCatalogue;
				catalogueSelected = catalogue;
			} else if (titleCatalogue.equals(title) && lastVersionSelected.compareTo(versionCatalogue) < 0) {
				lastVersionSelected = versionCatalogue;
				catalogueSelected = catalogue;
			}
		}
		return (catalogueSelected != null ? Utils.loadDocument(catalogueSelected.getAbsolutePath()) : null);
	}

	/**
	 * Obtiene un catalogo
	 * 
	 * @param title
	 *            Titulo del catalogo
	 * @param version
	 *            Version del catalogo
	 * @param path
	 *            Path de acceso
	 * @return catalogo
	 * @throws ConfigException
	 */
	public Document getCatalogue(String title, String version, String path) throws ConfigException {
		Document catalogue = this.searchCatalogue(title, version);
		this.setData(catalogue, path, version);
		return catalogue;
	}

	/**
	 * Obtiene la URL del grafo y la URL del servidor del catalogo
	 * 
	 * @param title
	 *            Titulo del catalogo
	 * @return URL del grafo y URL del servidor
	 * @throws ConfigException
	 */
	public String[] getConnectionCatalogue(String title, String version) throws ConfigException {
		Document catalogue = this.searchCatalogue(title, version);
		NodeList nodesPage = Utils.getNodes(catalogue, "/RDF/Description//*[local-name() = 'page']/@resource");
		String[] infoCatalogue = new String[2];
		infoCatalogue[0] = nodesPage.item(0).getTextContent();
		infoCatalogue[1] = nodesPage.item(1).getTextContent();
		return infoCatalogue;
	}

	/**
	 * Elimina un catalogo
	 * 
	 * @param title
	 *            Titulo del catalogo
	 * @param version
	 *            Version del catalogo
	 * @return true/false
	 */
	public boolean removeCatalogue(String title, String version) {
		File dirCatalogues = new File(DIR_CATALOGUES);
		File[] catalogues = dirCatalogues.listFiles();
		int length = catalogues.length;
		File catalogue = null;
		String titleCatalogue = "";
		String versionCatalogue = "";
		for (int i = 0; i < length; i++) {
			catalogue = catalogues[i];
			titleCatalogue = this.getTitleCatalogue(catalogue.getName());
			versionCatalogue = this.getVersionCatalogue(catalogue.getName());
			// Si no se especifica la version, se eliminar� la �ltima version
			if (!version.isEmpty() && titleCatalogue.equals(title) && versionCatalogue.equals(version)) {
				catalogue.delete();
				return true;
			} else if (titleCatalogue.equals(title)) {
				catalogue.delete();
				return true;
			}
		}

		return false;
	}

}
