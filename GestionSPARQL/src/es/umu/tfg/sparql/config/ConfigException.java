package es.umu.tfg.sparql.config;

/**
 * Encapsula los errores internos para generar el fichero de configuracion
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
public class ConfigException extends Exception {
	private static final long serialVersionUID = 1L;

	public ConfigException(String msg, Exception e) {
		super(msg, e);
	}

	public ConfigException(String msg) {
		super(msg);
	}

}
