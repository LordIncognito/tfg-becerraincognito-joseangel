package es.umu.tfg.sparql.config;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.namespace.NamespaceContext;
/**
 * Clase de apoyo para gestionar los espacios de nombres de la ontologia
 * @author Jose Angel Becerra Incognito
 *
 */
public class NameSpaces implements NamespaceContext {

	private Map<String, String> alias;

	public NameSpaces() {
		alias = new HashMap<String, String>();
		alias.put("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns");
		alias.put("rdfs", "http://www.w3.org/2000/01/rdf-schema");
		alias.put("owl", "http://www.w3.org/2002/07/owl");
	}

	@Override
	public String getNamespaceURI(String prefix) {
		return alias.get(prefix);
	}

	@Override
	public String getPrefix(String namespaceURI) {
		for (String entry : alias.keySet())
			if (alias.get(entry).equals(namespaceURI))
				return entry;
		return null;
	}

	@Override
	public Iterator<String> getPrefixes(String namespaceURI) {
		return alias.keySet().iterator();
	}

}
