package es.umu.tfg.sparql.parameters;

/**
 * Envoltorio para los parametros de ordenacion
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
public class BooleanParameters {
	private boolean shorted;
	private boolean distinct;
	private String shortedBy;

	public BooleanParameters() {
		this.shorted = false;
		this.distinct = false;
		this.shortedBy = "tipo";
	}

	public BooleanParameters(boolean shorted, String shortedBy) {
		this.shorted = shorted;
		this.shortedBy = shortedBy;
	}

	public BooleanParameters(BooleanParameters booleans) {
		this.shorted = booleans.shorted;
		this.shortedBy = booleans.shortedBy;
		this.distinct = booleans.distinct;
	}
	
	public boolean isShorted() {
		return shorted;
	}

	public void setShorted(boolean shorted) {
		this.shorted = shorted;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public String getShortedBy() {
		return shortedBy;
	}

	public void setShortedBy(String shortedBy) {
		this.shortedBy = shortedBy;
	}

}
