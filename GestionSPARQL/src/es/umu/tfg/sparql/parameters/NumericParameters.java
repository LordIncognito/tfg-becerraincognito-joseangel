package es.umu.tfg.sparql.parameters;

/**
 * Envoltorio para los parametros numericos
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
public class NumericParameters {
	private int limit;
	private int offset;

	public NumericParameters() {
		this.limit = -1;
		this.offset = -1;
	}

	public NumericParameters(int limit, int offset) {
		this.limit = limit;
		this.offset = offset;
	}

	public NumericParameters(NumericParameters numericParameters) {
		this.limit =numericParameters.limit;
		this.offset = numericParameters.offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

}
