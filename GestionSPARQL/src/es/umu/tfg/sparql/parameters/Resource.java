package es.umu.tfg.sparql.parameters;

/**
 * Clase envoltorio para almacenar los parametros que recibe el servicio REST
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
public class Resource implements Cloneable {
	private String graph;
	private String url;
	private Parameters parameters;

	public Resource() {
		this.graph = "";
		this.url = "";
		this.parameters = new Parameters();
	}

	public Resource(String graph, String url) {
		this.graph = graph;
		this.url = url;
		this.parameters = new Parameters();
	}

	public String getGraph() {
		return graph;
	}

	public void setGraph(String graph) {
		this.graph = graph;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Parameters getParameters() {
		return parameters;
	}

	public void setParameters(Parameters parameters) {
		this.parameters = parameters;
	}

	public String getValue() {
		return this.parameters.getValue();
	}

	public void setValue(String value) {
		this.parameters.setValue(value);
	}

	public String getFilter() {
		return this.parameters.getFilter();
	}

	public void setFilter(String filter) {
		this.parameters.setFilter(filter);
	}

	public String getFormat() {
		return this.parameters.getFormat();
	}

	public void setFormat(String format) {
		this.parameters.setFormat(format);
	}

	public boolean isSorted() {
		return this.parameters.isSorted();
	}

	public void setSorted(boolean sorted) {
		this.parameters.setSorted(sorted);
	}

	public String getSortedBy() {
		return this.parameters.getSortedBy();
	}

	public void setSortedBy(String sortedBy) {
		this.parameters.setSortedBy(sortedBy);
	}

	public int getLimit() {
		return this.parameters.getLimit();
	}

	public void setLimit(int limit) {
		this.parameters.setLimit(limit);
	}

	public int getOffset() {
		return this.parameters.getOffset();
	}

	public void setOffset(int offset) {
		this.parameters.setOffset(offset);
	}

	public boolean isDistinct() {
		return this.parameters.isDistinct();
	}

	public void setDistinct(boolean distinct) {
		this.parameters.setDistinct(distinct);
	}
	
	@Override
	public Resource clone() throws CloneNotSupportedException {
		Resource rs = null;
		rs = (Resource) super.clone();
		rs.parameters = new Parameters(this.parameters);
		return rs;
		
	}
}
