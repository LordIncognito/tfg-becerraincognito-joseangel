package es.umu.tfg.sparql.parameters;

/**
 * Envoltorio para los parametros opcionales del servicio
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
public class Parameters implements Cloneable {
	private String value;
	private String filter;
	private String format;
	private BooleanParameters booleanParameters;
	private NumericParameters numericParameters;

	public Parameters() {
		this.value = "";
		this.filter = "";
		this.format = "";
		this.booleanParameters = new BooleanParameters();
		this.numericParameters = new NumericParameters();
	}

	public Parameters(Parameters parameters) {
		this.filter = parameters.filter;
		this.value = parameters.value;
		this.format = parameters.format;
		this.booleanParameters = new BooleanParameters(parameters.booleanParameters);
		this.numericParameters = new NumericParameters(parameters.numericParameters);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public String getFormat() {
		return this.format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public BooleanParameters getBooleanParameters() {
		return booleanParameters;
	}

	public void setBooleanParameters(BooleanParameters booleanParameters) {
		this.booleanParameters = booleanParameters;
	}

	public NumericParameters getNumericParameters() {
		return numericParameters;
	}

	public void setNumericParameters(NumericParameters numericParameters) {
		this.numericParameters = numericParameters;
	}

	public void setSorted(boolean shorted) {
		this.booleanParameters.setShorted(shorted);
	}

	public void setSortedBy(String shortedBy) {
		this.booleanParameters.setShortedBy(shortedBy);
	}

	public boolean isSorted() {
		return this.booleanParameters.isShorted();
	}

	public boolean isDistinct() {
		return this.booleanParameters.isDistinct();
	}

	public void setDistinct(boolean distinct) {
		this.booleanParameters.setDistinct(distinct);
	}

	public String getSortedBy() {
		return this.booleanParameters.getShortedBy();
	}

	public void setLimit(int limit) {
		this.numericParameters.setLimit(limit);
	}

	public void setOffset(int offset) {
		this.numericParameters.setOffset(offset);
	}

	public int getLimit() {
		return this.numericParameters.getLimit();
	}

	public int getOffset() {
		return this.numericParameters.getOffset();
	}

	
	@Override
	protected Parameters clone() throws CloneNotSupportedException {
		Parameters para = null;
		para = (Parameters) super.clone();
		para.booleanParameters = new BooleanParameters(this.booleanParameters);
		para.numericParameters = new NumericParameters(this.numericParameters);
		return para;
	}
}
