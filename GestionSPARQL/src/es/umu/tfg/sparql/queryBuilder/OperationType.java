package es.umu.tfg.sparql.queryBuilder;

/**
 * Enumerado que indica las acciones posibles dentro de la clase QueryCreator
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
public enum OperationType {
	GET_GRAPHS, 
	GET_CLASSES,
	GET_PROPERTIES,
	GET_ALL,
	GET_ELEMENTS_BY_CLASS, 
	GET_ELEMENTS_BY_PROPERTY, 
	DESCRIBE_ELEMENT,
	GET_ELEMENTS_BY_VALUE,
	GET_COUNT_BY_CLASS,
	GET_COUNT_BY_PROPERTY;
}
