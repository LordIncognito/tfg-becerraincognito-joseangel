package es.umu.tfg.sparql.queryBuilder;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import es.umu.tfg.sparql.parameters.Resource;

/**
 * Clase encargada de analizar los parametros recibidos por el controlador
 * SPARQL y generar la sentencia adecuada
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
public class QueryBuilder {

	/**
	 * Genera la sentencia SPARQL para obtener todos los grafos que hay en la BD
	 * 
	 * @return sentencia SPARQL
	 */
	private String getGraphs() {
		return "SELECT DISTINCT ?graph \nWHERE{\nGRAPH ?graph {?s ?p ?o}\n}";
	}

	/**
	 * Metodo para obtener todas las condiciones definidas en el filtro
	 * 
	 * @param filtro
	 *            filtro
	 * @return condiciones
	 */
	private List<String> getConditions(String filtro) {
		List<String> condiciones = new LinkedList<String>();
		String[] conds = filtro.split("&&|\\|\\|");
		condiciones.addAll(Arrays.asList(conds));
		return condiciones;
	}

	/**
	 * Metodo para comprobar que la condicion no es una condicion de expresiones
	 * regulares
	 * 
	 * @param condicion
	 *            condicion
	 * @return true/false
	 */
	private boolean isRegularExpression(String condicion) {
		return condicion.startsWith("regex");
	}

	/**
	 * Metodo para obtener el atributo o propiedad a la que se aplica la expresion
	 * regular
	 * 
	 * @param condicion
	 *            condicion
	 * @return propiedad
	 */
	private String getSubjectRegularExpression(String condicion) {
		int posIni = condicion.indexOf("=");
		if (posIni < 0)
			return "Error: Regular expression invalid. The structure to define a filter regex is:\"regex=uri=regular_expresion\"\n";
		posIni++;
		int posEnd = condicion.lastIndexOf("=");
		if (posEnd < 0 || (posEnd - posIni) < 0)
			return "Error: Regular expression invalid. The structure to define a filter regex is: \"regex=uri=regular_expresion\"\n";
		String uri = condicion.substring(posIni, posEnd);
		if (!uri.matches("(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))
			return "Error: Regular expression invalid. The structure to define a filter regex is: \"regex=uri=regular_expresion\"\n";

		return uri;
	}

	/**
	 * Metodo para obtener la expresion regular de la condicion
	 * 
	 * @param condicion
	 *            Condicion
	 * @return expresion regular
	 */
	private String getRegularExpression(String condicion) {
		return condicion.substring(condicion.lastIndexOf("=") + 1, condicion.length());
	}

	/**
	 * Metodo para obtener el atributo o la propiedad de la condicion
	 * 
	 * @param condicion
	 *            Condicion booleana (Ej: http...#x < 10)
	 * @return propiedad (Ej: x)
	 */
	private String getDataTypeProperty(String condicion) {
		String separadores[] = { " ", "<", "<=", ">", ">=", "=" };
		int limiteNombre = 0;
		for (String separador : separadores) {
			if ((limiteNombre = condicion.indexOf(separador)) < 0)
				continue;
			return condicion.substring(0, limiteNombre);
		}
		return "";
	}

	/**
	 * Metodo para definir la tripleta
	 * 
	 * @param sujeto
	 *            Sujeto de la tripleta
	 * @param predicado
	 *            Predicado de la tripleta
	 * @param objeto
	 *            Objeto de la tripleta
	 * @return tripleta
	 */
	private String createTrio(String sujeto, String predicado, String objeto) {
		if (!sujeto.startsWith("?"))
			sujeto = "?" + sujeto;
		if (!objeto.startsWith("?"))
			objeto = "?" + objeto;

		return sujeto + " " + predicado + " " + objeto + " .\n";
	}

	/**
	 * Metodo para obtener la siguiente posicion el operador logico
	 * 
	 * @param filtro
	 *            conjunto de condiciones de filtrado
	 * @param pos
	 *            posicion del operador logico
	 * @return posicion
	 */
	private int getNextOperator(String filtro, int pos) {
		if (filtro.indexOf("&&", pos) >= 0)
			return filtro.indexOf("&&", pos);

		else if (filtro.indexOf("||", pos) >= 0)
			return filtro.indexOf("||", pos);

		return filtro.length();
	}

	/**
	 * Metodo para obtener el operador logico de la posicion X del conjunto de
	 * condiciones especificadas en el filtro
	 * 
	 * @param filtro
	 *            conjunto de condiciones de filtrado
	 * @param pos
	 *            posicion del operador logico
	 * @return && o ||
	 */
	private String getLogicOperator(String filtro, int pos) {
		if (filtro.indexOf("&&", pos) >= 0)
			return "&&";

		else if (filtro.indexOf("||", pos) >= 0)
			return "||";

		return "";
	}

	/**
	 * Obtiene el nombre del concepto
	 * 
	 * @param datatype
	 *            URL del concepto
	 * @return nombre
	 */
	private String getNameOfDatatype(String datatype) {
		int posAlmodilla = datatype.lastIndexOf("#") + 1;
		int length = datatype.length();
		return datatype.substring(posAlmodilla, length);
	}

	/**
	 * Metodo para definir los sujetos del SELECT de la sentencia SPARQL
	 * 
	 * @param condiciones
	 *            condiciones
	 * @return sujetos de la sentencia SPARQL
	 */
	private String getSubjects(List<String> condiciones) {
		String datatype = "";
		String query = "";
		String newSubjects = "";
		for (String condicion : condiciones) {
			if (this.isRegularExpression(condicion))
				continue;
			datatype = this.getDataTypeProperty(condicion);
			datatype = this.getNameOfDatatype(datatype);
			// Si ya esta el sujeto definido pues no se añade
			if (newSubjects.contains("?" + datatype))
				continue;
			newSubjects += " ?" + datatype;
		}
		query += newSubjects;
		return query;
	}

	/**
	 * Metodo para definir el conjunto de tripletas de la sentencia SPARQL para
	 * poder aplicar el filtro
	 * 
	 * @param tipo
	 *            Tipo o clase del elemento
	 * @param condiciones
	 *            Conjunto de condiciones para generar las tripletas
	 * @return tripletas SPARQL
	 */
	private String getTrios(String tipo, List<String> condiciones, String[] ordenacion) {
		String datatype = "";
		String query = "";
		String trio = "";
		String subjectRegex = "";
		String newTrios = "";
		if (condiciones != null && condiciones.size() > 0) {
			for (String condicion : condiciones) {
				if (this.isRegularExpression(condicion)) {
					subjectRegex = this.getSubjectRegularExpression(condicion);
					trio = this.createTrio(tipo, "<" + subjectRegex + ">", this.getNameOfDatatype(subjectRegex));
					if (newTrios.contains(trio))
						continue;
					newTrios += trio;
					continue;
				}

				datatype = this.getDataTypeProperty(condicion);
				if (!datatype.matches("(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))
					return "Error: filter must use URIs\n";
				trio = this.createTrio(tipo, "<" + datatype + ">", this.getNameOfDatatype(datatype));
				if (newTrios.contains(trio))
					continue;
				newTrios += trio;
			}
			query += newTrios;
		}
		newTrios = "";
		trio = "";
		String attr = "";
		if (ordenacion != null && ordenacion.length > 0) {
			for (String criterio : ordenacion) {
				if (criterio.equals("tipo"))
					continue;
				attr = this.getAttributeShort(criterio);
				if (!attr.matches("(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))
					return "Error: shortedBy must use URIs\n";
				trio = this.createTrio(tipo, "<" + attr + ">", this.getNameOfDatatype(attr));
				if (newTrios.contains(trio) || query.contains(trio))
					continue;

				newTrios += trio;
			}
			query += newTrios;
		}
		return query;
	}

	/**
	 * Metodo para generar la sentencia FILTER
	 * 
	 * @param filtro
	 *            Conjunto de condiciones para filtrar el resultado
	 * @param condiciones
	 *            Condiciones de filtrado
	 * @return sentencia FILTER
	 */
	private String getFilter(String filtro, List<String> condiciones) {
		int pos = 0;
		String query = "";
		String subjectExpression = "";
		for (String condicion : condiciones) {
			if (query.isEmpty()) {
				if (this.isRegularExpression(condicion)) {
					subjectExpression = this.getSubjectRegularExpression(condicion);
					subjectExpression = this.getNameOfDatatype(subjectExpression);
					query += " regex(?" + subjectExpression + ", \"" + this.getRegularExpression(condicion) + "\") "
							+ this.getLogicOperator(filtro, pos) + " ";
					pos = getNextOperator(filtro, pos) + 2;
				} else {
					query += " ?" + this.getNameOfDatatype(condicion) + " " + this.getLogicOperator(filtro, pos) + " ";
					pos = getNextOperator(filtro, pos) + 2;
				}
			} else if (this.isRegularExpression(condicion)) {
				subjectExpression = this.getSubjectRegularExpression(condicion);
				subjectExpression = this.getNameOfDatatype(subjectExpression);
				query += this.getLogicOperator(filtro, pos) + "regex(?" + subjectExpression + ", \""
						+ this.getRegularExpression(condicion) + "\") ";
				pos = getNextOperator(filtro, pos) + 2;
			} else {
				query += " ?" + this.getNameOfDatatype(condicion) + " " + this.getLogicOperator(filtro, pos) + " ";
				pos = getNextOperator(filtro, pos) + 2;
			}
		}
		return query;
	}
	
	private String addOrderByInFilter(String query, boolean sorted, String[] sortedBy) {
		if (sorted && sortedBy != null && sortedBy.length > 0 && !sortedBy[0].equals("tipo"))
			query += "ORDER BY " + this.getOrderBy(sortedBy);
		else if (sorted)
			query += "ORDER BY ?instance";
		
		return query;
	}

	/**
	 * Método para generar la sentencia SPARQL con filtrado
	 * 
	 * @param sujeto
	 *            sujeto principal
	 * @param objeto
	 *            Objeto principal
	 * @param filtro
	 *            fitlro
	 * @param uriTipo
	 *            Indicar que se usa URIs o no
	 * @return sentencia SPARQL
	 */
	private String buildQueryFilter(String sujeto, String objeto, String filtro, boolean sorted, String[] ordenacion, boolean uriTipo) {
		List<String> condiciones = this.getConditions(filtro);
		String query = "SELECT ?" + sujeto;
		// Definimos los sujetos del select
		query += this.getSubjects(condiciones);
		if (uriTipo)
			query += "\nWHERE{\n ?" + sujeto + " rdf:type ?tipo .\n" + "?tipo rdfs:subClassOf* " + objeto + " .\n";
		else
			query += "\nWHERE{\n ?" + sujeto + " rdfs:label ?label .\n";
		// Definimos las tripletas para poder aplicar el filtro
		query += this.getTrios(sujeto, condiciones, ordenacion);
		query = query + "FILTER(";
		if (!uriTipo)
			query += " regex(?label, \"" + objeto + "\") && ";
		// Definimos el filtro
		query += this.getFilter(filtro, condiciones);
		query = query + ")\n}\n";
		query = this.addOrderByInFilter(query, sorted, ordenacion);
		return query;
	}

	/**
	 * Método para generar la sentencia FILTER
	 * 
	 * @param filtro
	 *            Conjunto de condiciones para filtrar el resultado
	 * @return sentencia SPARQL con FILTER
	 */
	private String buildQueryFilter(String filtro) {
		List<String> condiciones = this.getConditions(filtro);
		String filter = this.getFilter(filtro, condiciones);
		return filter;
	}

	/**
	 * Método para generar la sentencia FILTER dependiendo del tipo
	 * 
	 * @param sujeto
	 *            Sujeto principald de la sentencia
	 * 
	 * @param predicado
	 *            Predicado principal de la tripleta principal
	 * 
	 * @param filtro
	 *            Conjunto de condiciones para filtrar el resultado
	 * @return sentencia SPARQL con FILTER
	 */
	private String buildQueryFilter(String sujeto, String predicado, String filtro, boolean sorted, String[] ordenacion) {
		List<String> condiciones = this.getConditions(filtro);
		String query = "SELECT ?" + sujeto;
		// Definimos los sujetos del select
		query += this.getSubjects(condiciones);
		if (predicado.matches("(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))
			predicado = "<" + predicado + ">";
		query += "\nWHERE{\n ?" + sujeto + " " + predicado + " ?valor .\n";
		// Definimos las tripletas para poder aplicar el filtro
		query += this.getTrios(sujeto, condiciones, ordenacion);
		query = query + "FILTER(";
		// Definimos el filtro
		query += this.getFilter(filtro, condiciones);
		query = query + ")\n}\n";
		query = this.addOrderByInFilter(query, sorted, ordenacion);
		return query;
	}

	private String getSubjectsShorted(String[] ordenacion) {
		String atributte = "";
		String nameAttribute = "";
		String subjects = "";
		for (String criterioOrden : ordenacion) {
			atributte = this.getAttributeShort(criterioOrden);
			nameAttribute = " ?" + this.getNameOfDatatype(atributte);
			if (subjects.contains(nameAttribute))
				continue;
			subjects += nameAttribute;
		}

		return subjects;
	}

	/**
	 * Crear una sentencia SPARQL con criterios de ordenacion
	 * 
	 * @param subject
	 *            Sujeto principal
	 * @param type
	 *            Elemento
	 * @param ordenacion
	 *            criterios de ordenacion
	 * @return sentencia SPARQL
	 */
	private String buildQueryShorted(String subject, String type, String[] ordenacion, boolean isProperty) {
		//System.out.println("EN BUILDQUERYSHORTED");
		if (!subject.startsWith("?"))
			subject = "?" + subject;
		String query = "SELECT " + subject + " ";
		// Añadimos los sujetos de ordenadcion
		query += this.getSubjectsShorted(ordenacion);
		if (!isProperty) {
			if (type.matches("(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))
				query += "\nWHERE{\n " + subject + " rdf:type ?tipo .\n" + "?tipo rdfs:subClassOf* <" + type + "> .\n";
			else
				query += "\nWHERE{\n " + subject + " rdfs:label ?label .\n";
		} else
			query += "\nWHERE{\n" + subject + " <" + type + "> ?valor .\n";

		// Añadimos las tripletas de ordenacion
		query += this.getTrios(subject, null, ordenacion);
		// Añadimos el fin de la sentencia
		query += "\n}\n";
		// Añadimos el ORDER BY
		query += "ORDER BY " + this.getOrderBy(ordenacion);
		return query;
	}

	/**
	 * Genera la sentencia SPARQL para obtener toda los tipos que hay en la BD,
	 * junto con su clase padre y etiqueta que tenga adjunta
	 * 
	 * @param filtro
	 *            Conjunto de condiciones para filtrar el resultado
	 * 
	 * @return sentencia SPARQL
	 */
	private String getClasses(String filtro, boolean shorted, String[] ordenacion) {

		String query = "SELECT ?class\n" + "WHERE\n" + "{\n" + "?class rdf:type owl:Class .\n}";
		if (!filtro.isEmpty())
			query = "SELECT ?class\n" + "WHERE\n" + "{\n" + "?class rdf:type owl:Class .\n"
					+ "FILTER "+ this.buildQueryFilter(filtro) + "\n}";

		if (shorted && ordenacion != null && ordenacion.length > 0 && !ordenacion[0].equals("tipo")) {
			query += "ORDER BY " + this.getOrderBy(ordenacion);
		} else if (shorted)
			query += "ORDER BY ?class";
		return query;
	}

	private String getProperties(String filtro, boolean shorted, String[] ordenacion) {
		String query = "SELECT DISTINCT ?property\n" + "WHERE {\n" + "{" + "SELECT ?property\n WHERE {\n"
				+ "?property rdf:type ?type .\n" + "?type rdfs:subPropertyOf* owl:ObjectProperty .\n}" + "} UNION {\n"
				+ "SELECT ?property WHERE {\n" + "?property rdf:type ?type .\n"
				+ "?type rdfs:subPropertyOf*  owl:DatatypeProperty .\n" + "}\n" + "}\n" + "}\n";

		if (!filtro.isEmpty()) {
			query = "SELECT DISTINCT ?property\n" + "WHERE {\n" + "{" + "SELECT ?property\n WHERE {\n"
					+ "?property rdf:type ?type .\n" + "?type rdfs:subPropertyOf* owl:ObjectProperty .\n"
					+ "} UNION {\n" + "SELECT ?property WHERE {\n" + "?property rdf:type ?type .\n"
					+ "?type rdfs:subPropertyOf*  owl:DatatypeProperty .\n" + "}\n" + "}\n" + "}\n"
					+ this.buildQueryFilter(filtro) + "}";
		}

		if (shorted && ordenacion != null && ordenacion.length > 0 && !ordenacion[0].equals("tipo")) {
			query += "ORDER BY " + this.getOrderBy(ordenacion);
		} else if (shorted)
			query += "ORDER BY ?property";

		return query;
	}

	private String getAll(String filtro, boolean shorted, String[] ordenacion) {
		String query = "SELECT DISTINCT *\n" 
				+ "WHERE {\n" 
				+ "{\n" + "SELECT ?class\nWHERE{\n"
				+ "?class rdf:type owl:Class .\n" + "}" + "} UNION {\n"
				+ "SELECT ?property\n WHERE {\n" + "?property rdf:type ?type .\n"
				+ "?type rdfs:subPropertyOf* owl:ObjectProperty .\n" + "}\n" + "} UNION {\n"
				+ "SELECT ?property WHERE {\n" + "?property rdf:type ?type .\n"
				+ "?type rdfs:subPropertyOf*  owl:DatatypeProperty .\n" + "}\n" + "}\n" + "}\n";

		if (!filtro.isEmpty()) {
			query = "SELECT DISTINCT *\n" 
					+ "WHERE {\n" + "{\n" 
					+ "SELECT ?class\nWHERE{\n"
					+ "?class rdf:type owl:Class .\n" + "}" + "} UNION {\n"
					+ "SELECT ?property\n WHERE {\n" + "?property rdf:type ?type .\n"
					+ "?type rdfs:subPropertyOf* owl:ObjectProperty .\n" + "} "
					+ "UNION {\n" + "SELECT ?property WHERE {\n"
					+ "?property rdf:type ?type .\n" + "?type rdfs:subPropertyOf*  owl:DatatypeProperty .\n" + "}\n"
					+ "}\n" + "}\n" + this.buildQueryFilter(filtro) + "}\n";
		}

		if (shorted && ordenacion != null && ordenacion.length > 0 && !ordenacion[0].equals("tipo")) {
			query += "ORDER BY " + this.getOrderBy(ordenacion);
		} else if (shorted)
			query += "ORDER BY ?class, ?property";

		return query;
	}

	/**
	 * Genera la sentencia SPARQL para obtener todas las instancias de la BD, del
	 * tipo pasado como parámetro
	 * 
	 * @param tipo
	 *            Tipo de elemento que se busca
	 * @param filtro
	 *            Conjunto de condiciones para filtrar el resultado
	 * @return sentencia SPARQL
	 */
	private String getElementsByType(String tipo, String filtro, boolean sorted, String[] ordenacion) {
		// Objeto principal de la tripleta principal para obtener los elementos
		String objeto = tipo;
		// Sujeto por defecto para almacenar los resultados de la consulta
		String sujeto = "instance";
		// Indica si el tipo especificado es una URI o una etiqueta
		boolean uriTipo = false;
		String query = "";
		// Si se consulta el tipo por su URI directamente pues ajustamos el select para
		// tener la URi
		if ((uriTipo = tipo.matches("(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))) {
			objeto = "<" + tipo + ">";

		}
		// Si se consulta por label, supuestamente
		else {
			query = "SELECT ?" + sujeto + "\n" + "WHERE{\n" + "?" + sujeto + " rdfs:label ?label .\n"
					+ "FILTER regex(?label, \"" + tipo + "\")}";
		}
		// Si es una URI pues lo ajustamos para URI
		if (uriTipo)
			query = "SELECT ?" + sujeto + "\n" + "WHERE{\n" + "?" + sujeto + " rdf:type ?tipo .\n"
					+ "?tipo rdfs:subClassOf* " + objeto + " .\n}";

		// Si es un filtro creamos el SELECT con el filtro
		if (!filtro.isEmpty()) {
			return this.buildQueryFilter(sujeto, objeto, filtro, sorted, ordenacion, uriTipo);
		}
		if (sorted && ordenacion != null && ordenacion.length > 0 && !ordenacion[0].equals("tipo"))
			return this.buildQueryShorted("instance", tipo, ordenacion, false);
		else if (sorted)
			query += "ORDER BY ?instance";
		return query;
	}

	/**
	 * Método para generar la sentencia SPARQL mediante la consulta de propiedades
	 * 
	 * @param tipo
	 *            propiedad
	 * @param filtro
	 *            filtro
	 * @return sentencia SPARQL
	 */
	private String getElementsByProperty(String tipo, String filtro, boolean sorted, String[] ordenacion) {
		// Predicado principal de la tripleta principal para obtener los elementos
		String predicado = tipo;
		// Sujeto por defecto para almacenar los resultados de la consulta
		String sujeto = "instance";
		String query = "";
		// Si es un filtro creamos el SELECT con el filtro
		if (!filtro.isEmpty()) {
			return this.buildQueryFilter(sujeto, predicado, filtro, sorted, ordenacion);
		}
		// Si se consulta el tipo por su URI directamente pues ajustamos el select para
		// tener la URi
		if (tipo.matches("(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")) {
			predicado = "<" + tipo + ">";
		}
		// Si se consulta por label, supuestamente
		else
			return "";
		query = "SELECT ?" + sujeto + "\n" + "WHERE{\n" + "?" + sujeto + " " + predicado + " ?valor .\n}";

		if (sorted && ordenacion != null && ordenacion.length > 0 && !ordenacion[0].equals("tipo"))
			return this.buildQueryShorted("instance", tipo, ordenacion, true);
		else if (sorted) {
			query += "ORDER BY ?instance";
		}
		
		return query;
	}

	/**
	 * Metodo para obtener el sentido de la ordenacion: DESC o ASC
	 * 
	 * @param criterio
	 *            criterio de ordenacion
	 * @return descendente o ascendente
	 */
	private String getShort(String criterio) {
		if (criterio.indexOf("=") < 0)
			return "ASC";
		String orden = criterio.substring(0, criterio.indexOf("="));
		if (orden.isEmpty())
			return "ASC";
		else if (orden.equals("DESC") || orden.equals("ASC"))
			return orden;

		return "";
	}

	/**
	 * Método para obtener el atributo por el que se ordenara el resultado
	 * 
	 * @param criterio
	 *            criterio de ordenacion
	 * @return atributo de ordenacion
	 */
	private String getAttributeShort(String criterio) {
		if (criterio.lastIndexOf("=") < 0)
			return criterio;
		return criterio.substring(criterio.lastIndexOf("=") + 1, criterio.length());
	}

	/**
	 * Método generar la sentencia ORDER BY
	 * 
	 * @param criterioOrdenar
	 *            Criterios de ordenacion
	 * @return sentencia ORDER BY
	 */

	private String getOrderBy(String[] criterioOrdenar) {
		String orderBy = "";
		String criterio = "";
		int length = criterioOrdenar.length;
		for (int i = 0; i < length - 1; i++) {
			criterio = criterioOrdenar[i];
			if (!criterio.isEmpty()) {
				String orden = this.getShort(criterio);
				String atributo = this.getAttributeShort(criterio);
				if (atributo.isEmpty())
					return "";
				if (!atributo.matches("(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))
					return "Error: shortedBy must use URIs";
				atributo = this.getNameOfDatatype(atributo);
				orderBy += orden + "(?" + atributo + "),";
			}
		}
		criterio = criterioOrdenar[length - 1];
		if (!criterio.isEmpty()) {
			String orden = this.getShort(criterio);
			if (length == 1) {
				String atributo = this.getAttributeShort(criterio);
				if (!atributo.matches("(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))
					return "Error: shortedBy must use URIs\n";
				atributo = this.getNameOfDatatype(atributo);
				orderBy = orden + "(?" + atributo + ")";
			} else {
				String atributo = this.getAttributeShort(criterio);
				if (!atributo.matches("(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))
					return "Error: shortedBy must use URIs\n";
				atributo = this.getNameOfDatatype(atributo);

				orderBy += orden + "(?" + atributo + ")";
			}
		}
		return orderBy;
	}

	/**
	 * Metodo para comprobar que los criterios de ordenacion son correctos y estan
	 * definidos en la sentencia SPARQL
	 * 
	 * @param query
	 *            Sentencia SPARQL
	 * @param criterioOrdenar
	 *            criterios de ordenacion
	 * @return true/false
	 */
	private boolean isValidShortedConditions(String query, String[] criterioOrdenar) {
		String criterio = "";
		int length = criterioOrdenar.length;
		for (int i = 0; i < length; i++) {
			criterio = criterioOrdenar[i];
			if (!criterio.isEmpty()) {
				String variable = "?" + this.getNameOfDatatype(this.getAttributeShort(criterio));
				if (!query.contains(variable))
					return false;
			}
		}
		return true;
	}

	private String getDescribeElement(String tipo) {
		return "DESCRIBE <" + tipo + ">";
	}

	private String parseToReal(String value) {
		try {
			Double.parseDouble(value);
			return "\"" + value + "\"^^xsd:float";
		} catch (NumberFormatException e) {

		}
		return "";
	}

	private String parseToInt(String value) {
		try {
			Integer.parseInt(value);
			return "\"" + value + "\"^^xsd:integer";
		} catch (NumberFormatException e) {

		}
		return "";
	}

	private String formatValue(String value) {
		String number = this.parseToReal(value);
		if (!number.isEmpty())
			return number;
		number = this.parseToInt(value);
		if (!number.isEmpty())
			return number;

		if (value.matches("(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))
			return "<" + value + ">";

		return "\"" + value + "\"^^xsd:string";
	}

	private String buildQueryShortedInstancesValue(String uriProperty, String value, String[] ordenacion) {
		String query = "SELECT ?uri ?type";
		query += this.getSubjectsShorted(ordenacion);
		query += "\nWHERE{\n";
		if (uriProperty.startsWith("http"))
			query += "?uri <" + uriProperty + "> " + this.formatValue(value) + " .\n";
		else
			query += "?uri " + uriProperty + " " + this.formatValue(value) + " .\n";
		query += this.getTrios("?uri", null, ordenacion);
		query += "\nOPTIONAL { ?uri rdf:type ?type.}\n" + "\n" + "}\n";
		query += this.getOrderBy(ordenacion);

		return query;
	}

	private String getInstancesByValue(String uriProperty, String value, boolean shorted, String[] ordenacion) {
		String query = "";
		if (uriProperty.startsWith("http"))
			query = "SELECT ?uri ?type\n" + "WHERE{\n" + "?uri <" + uriProperty + "> " + this.formatValue(value) + ".\n"
					+ "OPTIONAL { ?uri rdf:type ?type.}\n" + "\n" + "}\n";

		else
			query = "SELECT ?uri ?type\n" + "WHERE{\n" + "?uri " + uriProperty + " " + this.formatValue(value) + ".\n"
					+ "OPTIONAL { ?uri rdf:type ?type.}\n" + "\n" + "}\n";

		if (shorted && ordenacion != null && ordenacion.length > 0 && !ordenacion[0].equals("tipo"))
			return this.buildQueryShortedInstancesValue(uriProperty, value, ordenacion);
		else if (shorted)
			query += "ORDER BY ?uri, ?type";

		return query;
	}

	private String buildCountQueryFilter(String subject, String object, String filter, boolean uriTipo) {
		List<String> condiciones = this.getConditions(filter);

		String query = "SELECT COUNT(?" + subject + ") AS ?instances";
		if (uriTipo)
			query += "\nWHERE{\n ?instance rdf:type ?tipo .\n" + "?tipo rdfs:subClassOf* <" + object + "> .\n";
		else
			query += "\nWHERE{\n ?instance rdfs:label ?label .\n";

		// Definimos las tripletas para poder aplicar el filtro
		query += this.getTrios(subject, condiciones, null);
		query = query + "FILTER(";
		if (!uriTipo)
			query += " regex(?label, \"" + object + "\") && ";
		// Definimos el filtro
		query += this.getFilter(filter, condiciones);
		query = query + ")\n}";

		return query;
	}

	private String getCountInstancesByClass(String type, String filter) {

		String object = type;
		String query = "";
		if (object.matches("(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")) {
			query = "SELECT COUNT(?instance) AS ?instances\n" + "WHERE\n" + "{\n" + "?instance rdf:type <" + type
					+ "> .\n" + "}\n";

			if (!filter.isEmpty())
				query = this.buildCountQueryFilter("instance", object, filter, true);
		} else {
			query = "SELECT  COUNT(?instance) AS ?instances\n" + "WHERE{\n" + "?instance rdfs:label ?label .\n"
					+ "FILTER regex(?label, \"" + object + "\"}";

			if (!filter.isEmpty())
				query = this.buildCountQueryFilter("instance", object, filter, false);
		}

		return query;
	}

	private String getCountInstancesByProperty(String type, String filter) {
		String query = "SELECT DISTINCT COUNT(?instance) AS ?instances\n" + "WHERE\n" + "{\n" + "?instance <" + type
				+ "> ?value .\n" + "}\n";

		if (!filter.isEmpty())
			query = "SELECT DISTINCT COUNT(?instance) AS ?instances\n" + "WHERE\n" + "{\n" + "?instance <" + type
					+ "> ?value .\n" + this.buildQueryFilter(filter) + "}\n";

		return query;
	}

	/**
	 * Método principal que retorna la sentencia SPARQL deseada
	 * 
	 * @param resource
	 *            Parametros para generar las sentencia
	 * 
	 * @return sentencia SPARQL
	 */
	public String getQuery(OperationType operacion, Resource resource) {
		String query = "";
		String tipo = resource.getUrl();
		String id = "";
		String value = resource.getValue();
		boolean ordenado = resource.isSorted();
		boolean distinct = resource.isDistinct();
		String filtro = resource.getFilter();
		String ordenadoPor = resource.getSortedBy();
		int limite = resource.getLimit();
		int offset = resource.getOffset();
		String[] criterioOrdenar = ordenadoPor.split(",");
		//System.out.println("OPERACION SOLICITADA: " + operacion);
		//System.out.println("TIPO = " + tipo);
		//System.out.println("ID = " + id);
		//System.out.println("VALUE = " + value);
		//System.out.println("ORDENADO = " + ordenado);
		//System.out.println("DISTINCT = " + distinct);
		//System.out.println("FILTRO = " + filtro);
		//System.out.println("ORDENADO-POR = " + ordenadoPor);
		switch (operacion) {
		case GET_GRAPHS:
			query = this.getGraphs();
			if (ordenado)
				query += "\nORDER BY ?graph";
			break;
		case GET_CLASSES:
			query = this.getClasses(filtro, ordenado, criterioOrdenar);
			if (ordenado && !ordenadoPor.equals("tipo") && !this.isValidShortedConditions(query, criterioOrdenar))
				return "";
			break;
		case GET_PROPERTIES:
			query = this.getProperties(filtro, ordenado, criterioOrdenar);
			if (ordenado && !ordenadoPor.equals("tipo") && !this.isValidShortedConditions(query, criterioOrdenar))
				return "";
			break;
		case GET_ALL:
			query = this.getAll(filtro, ordenado, criterioOrdenar);
			if (ordenado && !ordenadoPor.equals("tipo") && !this.isValidShortedConditions(query, criterioOrdenar))
				return "";
			break;
		case DESCRIBE_ELEMENT:
			query = this.getDescribeElement(tipo);
			break;
		case GET_ELEMENTS_BY_CLASS:
			query = this.getElementsByType(tipo, filtro, ordenado, criterioOrdenar);
			if (!ordenadoPor.equals("tipo") && ordenado && !this.isValidShortedConditions(query, criterioOrdenar))
				return "";
			break;
		case GET_ELEMENTS_BY_PROPERTY:
			query = this.getElementsByProperty(tipo, filtro, ordenado, criterioOrdenar);
			if (!ordenadoPor.equals("tipo") && ordenado && !this.isValidShortedConditions(query, criterioOrdenar))
				return "";
			break;
		case GET_ELEMENTS_BY_VALUE:
			query = this.getInstancesByValue(tipo, value, ordenado, criterioOrdenar);
			if (!ordenadoPor.equals("tipo") && ordenado && !this.isValidShortedConditions(query, criterioOrdenar))
				return "";
			break;
		case GET_COUNT_BY_CLASS:
			query = this.getCountInstancesByClass(tipo, filtro);
			break;

		case GET_COUNT_BY_PROPERTY:
			query = this.getCountInstancesByProperty(tipo, filtro);
			break;

		}
		if (distinct && !query.contains("DISTINCT")) {
			int posSelect = query.indexOf("SELECT") + 6;
			query = query.substring(0, posSelect) + " DISTINCT" + query.substring(posSelect);
		}
		if (limite > 0)
			query += "\nLIMIT " + Integer.toString(limite);
		if (offset > 0)
			query += "\nOFFSET " + Integer.toString(offset);
		//System.out.println("QUERY= " + query);
		return query;
	}

}
