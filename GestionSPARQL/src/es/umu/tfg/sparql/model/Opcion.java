package es.umu.tfg.sparql.model;

import javax.xml.bind.annotation.XmlAccessorType;

import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Clase que representa que opción disponible tiene el usuario en el servicio
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "id", "title", "updated", "author", "link" })
public class Opcion extends Atom {
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(required = true)
	private List<Link> link;

	public void setLink(List<Link> link) {
		this.link = link;
	}

	public List<Link> getLink() {
		if (link == null)
			link = new LinkedList<Link>();
		return link;
	}

	public void add(String href) {
		if (link == null)
			link = new LinkedList<Link>();

		this.link.add(Link.fromUri(href).build());
	}

	public void add(String href, String type) {
		if (link == null)
			link = new LinkedList<Link>();

		this.link.add(Link.fromUri(href).type(type).build());
	}

	public void add(String rel, String type, String href) {
		if (link == null)
			link = new LinkedList<Link>();

		this.link.add(Link.fromUri(href).rel(rel).type("GET").build());
	}

	private String getQueryParam(String href) {
		int posInterrogacion = href.indexOf("?");
		if (posInterrogacion < 0)
			return "";

		int posIgual = href.indexOf("=", posInterrogacion) + 1;
		return href.substring(posIgual, href.length());
	}

	public JSONObject toJSON() throws JSONException {
		JSONObject json = new JSONObject();
		JSONObject jsonField = null;
		JSONObject versions = new JSONObject();		
		for (Link l : link) {
			jsonField = new JSONObject();
			jsonField.put("href", l.getUri().toString());
			versions.put(this.getQueryParam(l.getUri().toString()), jsonField);
		}
		json.put(this.title, versions);
		return json;
	}
}
