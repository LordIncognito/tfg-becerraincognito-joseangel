
package es.umu.tfg.sparql.model;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Clase XML para mostrar un menu de ayuda al usuario
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "id", "title", "updated", "author", "opcion" })
@XmlRootElement(name = "feed")
public class MenuGrafos extends Atom {
	@XmlElement(name = "entry")
	private List<Opcion> opcion;

	public List<Opcion> getOpciones() {
		if (opcion == null)
			opcion = new LinkedList<Opcion>();
		return opcion;
	}

	public void setOpciones(List<Opcion> opcion) {
		this.opcion = opcion;
	}

	private Opcion existsOpcion(String title) {
		if (opcion == null)
			this.opcion = new LinkedList<Opcion>();
		Opcion op = null;
		try {
			op = opcion.stream().filter(o -> o.getTitle().equals(title)).findFirst().get();
		} catch (Exception e) {
			op = null;
		}
		return op;
	}

	public void add(String id, String title, XMLGregorianCalendar fecha, String author, String path) {
		Opcion op = null;
		if ((op = this.existsOpcion(title)) != null) {
			op.add(path, "GET");
			return;
		}
		op = new Opcion();
		op.setId(id);
		op.setTitle(title);
		op.setUpdated(fecha);
		op.setAuthor(author);
		op.add(path, "GET");

		this.opcion.add(op);
	}

	public String toJSON() throws JSONException {
		JSONArray links = new JSONArray();
		for (Opcion op : opcion) {
			links.put(op.toJSON());
		}
		JSONObject json = new JSONObject();
		json.put("_links", links);
		return json.toString(5);
	}

}
