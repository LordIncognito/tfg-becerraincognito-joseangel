package es.umu.tfg.sparql.model;

import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Representa una propiedad (ObjectProperty o DatatypeProperty) de la ontologia
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "id", "title", "updated", "author", "category", "uri", "link" })
public class Propiedad extends Atom {
	@XmlElement
	private Category category;
    @XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(name = "link", required = true)
	private Link uri;
    @XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(required = true)
	private List<Link> link;

	public Category getCategory() {
		if (category == null)
			category = new Category();
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setCategory(String term) {
		if (category == null)
			category = new Category();
		category.setTerm(term);
	}

	public Link getUri() {
		return uri;
	}

	public void setUri(Link uri) {
		this.uri = uri;
	}

	public void setUri(String href) {
		if (uri == null)
			uri = Link.fromUri(href).type("GET").build();
	}

	public void setUri(String rel, String href, String type) {
		if (uri == null)
			uri = Link.fromUri(href).rel(rel).type(type).build();
	}

	public List<Link> getLink() {
		if (link == null)
			link = new LinkedList<Link>();
		return link;
	}

	public void setLink(List<Link> link) {
		this.link = link;
	}

	public void add(String rel,String href, String type) {

		if (link == null)
			link = new LinkedList<Link>();
		
		this.link.add(Link.fromUri(href).rel(rel).type(type).build());
	}
	
	
	private String getFormatOfLink(String href) {
		int posInterrogacion = href.indexOf("?");
		if (posInterrogacion < 0)
			return "text/turtle";

		int posIgual = href.indexOf("=", posInterrogacion) + 1;
		return href.substring(posIgual, href.length());
	}
	
	private String getURI(String href) {
		int length = href.length();
		int posHTTP = href.lastIndexOf("http", length);
		int posInterrogacion = href.indexOf("?");
		int limite = 0;
		if (posInterrogacion < 0) limite = length;
		else limite = posInterrogacion;
		return href.substring(posHTTP, limite);
	}

	public JSONObject toJSON() throws JSONException {
		JSONArray arrayLinks = new JSONArray();
		JSONObject jsonField = null;
		JSONObject json = new JSONObject();
		for(Link l : link) {
			jsonField =  new JSONObject();
			jsonField.put("href", l.getUri().toString());
			jsonField.put("type", this.getFormatOfLink(l.getUri().toString()));
			arrayLinks.put(jsonField);
		}
		json.put(this.getURI(link.get(0).getUri().toString()), arrayLinks);
		return json;
	}

}
