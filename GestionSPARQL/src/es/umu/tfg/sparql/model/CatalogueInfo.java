package es.umu.tfg.sparql.model;

import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Clase XML para mostrar un menu de entrada de catalogos
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "id", "title", "updated", "author", "link" })
@XmlRootElement(name = "feed")
public class CatalogueInfo extends Atom {
	@XmlJavaTypeAdapter(Link.JaxbAdapter.class)
	@XmlElement(required = true)
	private List<Link> link;

	public List<Link> getLink() {
		if (link == null)
			link = new LinkedList<Link>();
		return link;
	}

	public void setLink(List<Link> link) {
		this.link = link;
	}

	public void add(String rel, String href, String type) {
		if (link == null)
			link = new LinkedList<Link>();

		this.link.add(Link.fromUri(href).rel(rel).type(type).build());
	}

	private String getFormatOfLink(String href) {
		int posInterrogacion = href.indexOf("?");
		if (posInterrogacion < 0)
			return "text/turtle";

		int posIgual = href.indexOf("=", posInterrogacion) + 1;
		int posAnd = href.indexOf("&", posInterrogacion);
		int limite = (posAnd < 0 ? href.length() : posAnd);
		return href.substring(posIgual, limite);
	}

	public String toJSON() throws JSONException {
		JSONObject jsonField = null;
		String nameLink = "link_";
		int i = 1;
		JSONObject links = new JSONObject();
		for (Link l : link) {
			jsonField = new JSONObject();
			jsonField.put("href", l.getUri().toString());
			jsonField.put("type", this.getFormatOfLink(l.getUri().toString()));
			links.put(nameLink + Integer.toString(i), jsonField);
			i++;
		}
		JSONObject json = new JSONObject();
		json.put("_links", links);
		return json.toString();
	}

}
