@XmlSchema(
		namespace="http://www.w3.org/2005/Atom",
		elementFormDefault = XmlNsForm.QUALIFIED,
		xmlns = {
				@XmlNs(prefix="", namespaceURI="http://www.w3.org/2005/Atom"),
				@XmlNs(prefix="rdfs", namespaceURI="http://www.w3.org/2000/01/rdf-schema")
		}

)

/**
 * Paquete que contiene todo el modelo del menu de ayuda
 */
package es.umu.tfg.sparql.model;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;



