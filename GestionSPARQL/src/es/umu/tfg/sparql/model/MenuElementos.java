package es.umu.tfg.sparql.model;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Clase principal que representa todos los conceptos consultables del grafo
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "id", "title", "updated", "author", "clase", "propiedad" })
@XmlRootElement(name = "feed")
public class MenuElementos extends Atom {
	@XmlElement(name = "entry", required = true)
	private List<Clase> clase;

	@XmlElement(name = "entry", required = true)
	private List<Propiedad> propiedad;

	public List<Clase> getClase() {
		if (clase == null)
			clase = new LinkedList<Clase>();
		return clase;
	}

	public void setClase(List<Clase> clase) {
		this.clase = clase;
	}

	public List<Propiedad> getPropiedad() {
		if (propiedad == null)
			propiedad = new LinkedList<Propiedad>();
		return propiedad;
	}

	public void setPropiedad(List<Propiedad> propiedad) {
		this.propiedad = propiedad;
	}

	public void addPropiedad(String id, String title, XMLGregorianCalendar fecha, String author, String uriPropiedad,
			String pathPropiedad, String category, String[] formatos, String[] describeFormats) {
		if (propiedad == null)
			propiedad = new LinkedList<Propiedad>();
		Propiedad prop = new Propiedad();
		prop.setId(id);
		prop.setTitle(title);
		prop.setUpdated(fecha);
		prop.setAuthor(author);
		prop.setUri(uriPropiedad);
		prop.setCategory(category);
		String relValue = "";
		String queryParam = "";
		for (String formato : formatos) {
			if (!pathPropiedad.contains("describe") && !formato.equals("text/turtle") && Arrays.asList(describeFormats).contains(formato))
				continue;
			relValue = (formato.equals("text/turtle") ? "" : "alternate");
			queryParam = (formato.equals("text/turtle") ? "" : "?format=" + formato);
			queryParam = (!formato.contains("+") ? queryParam : "?format=" + formato.replaceAll("\\+", "%2b"));
			prop.add(relValue, pathPropiedad + queryParam, formato);
		}
		this.propiedad.add(prop);
	}

	public void addClase(String id, String title, XMLGregorianCalendar fecha, String author, String uriClase,
			String label, String pathClase, String[] formatos, String[] describeFormats) {
		if (clase == null)
			clase = new LinkedList<Clase>();
		Clase newClase = new Clase();
		newClase.setId(id);
		newClase.setTitle(title);
		newClase.setUpdated(fecha);
		newClase.setAuthor(author);
		newClase.setUri(uriClase);
		newClase.setCategory("owl:Class");
		if (!label.isEmpty())
			newClase.setLabel(label);
		String relValue = "";
		String queryParam = "";
		for (String formato : formatos) {
			if (!pathClase.contains("describe") && !formato.equals("text/turtle") && Arrays.asList(describeFormats).contains(formato))
				continue;
			else if (pathClase.contains("describe") && Arrays.asList(describeFormats).contains(formato)) {
				relValue = (formato.equals("text/turtle") ? "" : "alternate");
				queryParam = (formato.equals("text/turtle") ? "" : "?format=" + formato);
				queryParam = (!formato.contains("+") ? queryParam : "?format=" + formato.replaceAll("\\+", "%2b"));
				newClase.add(relValue, pathClase + queryParam, formato);
			} else {
				relValue = (formato.equals("text/turtle") ? "" : "alternate");
				queryParam = (formato.equals("text/turtle") ? "" : "?format=" + formato);
				queryParam = (!formato.contains("+") ? queryParam : "?format=" + formato.replaceAll("\\+", "%2b"));
				newClase.add(relValue, pathClase + queryParam, formato);
			}
		}
		this.clase.add(newClase);
	}

	public void addClase(String id, String title, XMLGregorianCalendar fecha, String author, String uriClase,
			String label, String pathClase, String[] formatos) {
		if (clase == null)
			clase = new LinkedList<Clase>();
		Clase newClase = new Clase();
		newClase.setId(id);
		newClase.setTitle(title);
		newClase.setUpdated(fecha);
		newClase.setAuthor(author);
		newClase.setUri(uriClase);
		newClase.setCategory("owl:Class");
		if (!label.isEmpty())
			newClase.setLabel(label);
		String relValue = "";
		String queryParam = "";
		for (String formato : formatos) {
			relValue = (formato.equals("text/turtle") ? "" : "alternate");
			queryParam = (formato.equals("text/turtle") ? "" : "?format=" + formato);
			queryParam = (!formato.contains("+") ? queryParam : "?format=" + formato.replaceAll("\\+", "%2b"));
			newClase.add(relValue, pathClase + queryParam, formato);
		}
		this.clase.add(newClase);
	}

	public String toJSON(boolean onlyClasses, boolean onlyProperties) throws JSONException {

		JSONObject data = new JSONObject();
		JSONArray classes = new JSONArray();
		if (!onlyProperties) {
			for (Clase c : clase) {
				classes.put(c.toJSON());
			}
			data.put("classes", classes);
		}
		JSONArray properties = new JSONArray();

		if (!onlyClasses) {
			for (Propiedad p : propiedad) {
				properties.put(p.toJSON());
			}
			data.put("properties", properties);
		}
		JSONObject json = new JSONObject();
		json.put("_links", data);
		return json.toString();
	}

}
