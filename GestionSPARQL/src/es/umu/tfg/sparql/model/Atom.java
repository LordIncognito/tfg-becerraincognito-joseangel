package es.umu.tfg.sparql.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * 
 * Clase principal del modelado donde definimos los campos obligatorios de la
 * estructura ATOM
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
@XmlTransient
public abstract class Atom {
	@XmlElement(required = true)
	protected String id;
	@XmlElement(required = true)
	protected String title;
	@XmlElement(required = true)
	protected XMLGregorianCalendar updated;
	@XmlElement(required = true)
	protected String author;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public XMLGregorianCalendar getUpdated() {
		return updated;
	}

	public void setUpdated(XMLGregorianCalendar updated) {
		this.updated = updated;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

}
