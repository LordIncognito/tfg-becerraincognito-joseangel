package es.umu.tfg.sparql.controller;

/**
 * Encapsula los errores internos del controlador SPARQL
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
public class SparqlException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public SparqlException(String msg, Exception e) {
		super(msg, e);
	}

	public SparqlException(String msg) {
		super(msg);
	}

}
