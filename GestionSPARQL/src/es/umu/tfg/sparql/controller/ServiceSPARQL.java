package es.umu.tfg.sparql.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.XMLGregorianCalendar;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.json.JSONException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import es.umu.tfg.sparql.config.ConfigException;
import es.umu.tfg.sparql.config.ControllerSPARQLConfig;
import es.umu.tfg.sparql.config.Utils;
import es.umu.tfg.sparql.model.CatalogueInfo;
import es.umu.tfg.sparql.model.MenuElementos;
import es.umu.tfg.sparql.model.MenuGrafos;
import es.umu.tfg.sparql.parameters.Resource;
import es.umu.tfg.sparql.queryBuilder.QueryBuilder;

import es.umu.tfg.sparql.queryBuilder.OperationType;

/**
 * Controlador SPARQL encargado de tratar las peticiones del servicio REST
 * 
 * @author Jose Angel Becerra Incognito
 *
 */
public class ServiceSPARQL {
	private static ServiceSPARQL uniqueInstance;

	private ServiceSPARQL() {
	}

	public static ServiceSPARQL getUniqueInstance() {
		if (uniqueInstance == null)
			uniqueInstance = new ServiceSPARQL();
		return uniqueInstance;
	}

	/**
	 * Método para crear la URL del servidor SPARQL
	 * 
	 * @param grafo
	 *            Grafo en el que se consulta
	 * @param query
	 *            Sentencia SPARQL
	 * @param formato
	 *            Formato solicitado por el cliente
	 * @return URL
	 * @throws SparqlException
	 */
	private String createURL(String server, String graph, String query, String format) throws SparqlException {
		String url = "";
		try {
			url = server + "?" + "default-graph-uri=" + Utils.cadenaURLEncoder(graph, "UTF-8") + "&query="
					+ Utils.cadenaURLEncoder(query, "UTF-8") + "&format=" + Utils.cadenaURLEncoder(format, "UTF-8");
		} catch (ConfigException e) {
			throw new SparqlException("Error to create URL", e);
		}
		return url;
	}

	/**
	 * Método para generar el documento RDF
	 * 
	 * @param grafo
	 *            Grafo en el que se consulta
	 * @param query
	 *            Sentencia SPARQL
	 * @return documento RDF
	 * @throws SparqlException
	 */
	private Document generarDocumento(String server, String graph, String query, String format) throws SparqlException {
		Document documento = null;
		String url = this.createURL(server, graph, query, format);
		try {
			documento = Utils.loadDocument(url);
		} catch (ConfigException e) {
			throw new SparqlException("Fail to create the document", e);
		}
		return documento;

	}

	/**
	 * Método para generar la respuesta del servidor SPARQL en formato TURTLE o JSON
	 * 
	 * @param grafo
	 *            Grafo en el que se consulta
	 * @param query
	 *            Sentencia SPARQL
	 * @param formato
	 *            Formato solicitado
	 * @return resultado consulta
	 * @throws SparqlException
	 */
	private String generarRespuesta(String server, String graph, String query, String format) throws SparqlException {
		StringBuilder response = new StringBuilder();
		URL url;
		try {
			url = new URL(server + "?" + "default-graph-uri=" + Utils.cadenaURLEncoder(graph, "UTF-8") + "&query="
					+ Utils.cadenaURLEncoder(query, "UTF-8") + "&format=" + Utils.cadenaURLEncoder(format, "UTF-8"));
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
				response.append(System.lineSeparator());
			}
			in.close();
		} catch (MalformedURLException e) {
			throw new SparqlException("Error to create URL of the resource SPARQL");
		} catch (IOException e) {
			throw new SparqlException("Resource not found");
		} catch (ConfigException e) {
			throw new SparqlException("Error to codify URL", e);
		}

		return response.toString();
	}

	/**
	 * Método para comprobar si la información solicitada por el cliente existe y si
	 * tiene acceso
	 * 
	 * 
	 * @param nameServer
	 *            Nombre del servidor
	 * @param graph
	 *            Grafo donde se consulta
	 * 
	 * @return true/false
	 * @throws SparqlException
	 */
	private boolean isValidQuery(String nameServer, String graph) throws SparqlException {
		boolean valida = true;
		try {
			valida = ControllerSPARQLConfig.getUniqueInstance().isValidQuery(nameServer, graph);
		} catch (ConfigException e) {
			throw new SparqlException("Error to check if query is valid", e);
		}
		return valida;
	}

	/**
	 * Método para obtener la URL del grafo que se consulta
	 * 
	 * @param grafo
	 *            Grafo donde se consulta
	 * 
	 * @return URL
	 * @throws SparqlException
	 */
	private String getGrafoURI(String nameServer, String grafo) throws SparqlException {
		try {
			return ControllerSPARQLConfig.getUniqueInstance().getGrafoURI(nameServer, grafo);
		} catch (ConfigException e) {
			throw new SparqlException("Fail to get URL of the graph", e);
		}
	}

	
	/**
	 * Obtiene el mensaje de error del QueryBuilder
	 * 
	 * @param query
	 *            Sentencia SPARQL incorrecta
	 * @return mensaje de error
	 */
	private String getError(String query) {
		String er = "Error:.+\\s{1}";
		Pattern p = Pattern.compile(er);
		Matcher m = p.matcher(query);
		String msgError = (m.find() ? m.group()
				: "The SPARQL sentence can´t be created correctly. Check if you specify the right resource or the parameters are correctly");

		if (msgError.startsWith("Error"))
			msgError = msgError.substring(msgError.indexOf(":") + 1, msgError.length());

		return msgError;
	}

	/**
	 * Método principal, que se encarga de realizar la consulta SPARQL y devolver el
	 * resultado en el formato especificado como parámetro
	 * 
	 * @param operacion
	 *            Operacion a realizar
	 * @param resource
	 *            Parametros de la consulta
	 * @return resultado consulta
	 * @throws SparqlException
	 */
	public Document querySPARQL(OperationType operacion, Resource resource) throws SparqlException {

		String server = this.getServerGraph(resource.getGraph());
		if (server == null || server.isEmpty())
			throw new SparqlException("The graph not exists");
		String nameServer = this.getNameServer(server);
		Document rdf = null;
		QueryBuilder builder = new QueryBuilder();
		String graph = "";
		if (operacion.equals(OperationType.GET_GRAPHS)) {
			String query = builder.getQuery(operacion, resource);
			if (query.isEmpty())
				throw new SparqlException(
						"The SPARQL sentence can´t be created correctly. Check if you specify the right resource or the parameters are correctly");
			rdf = this.generarDocumento(server, resource.getGraph(), query, resource.getFormat());
			if (rdf == null)
				throw new SparqlException("The response is incorrect. Check the identifier of the resource is correct");
		}

		else if (this.isValidQuery(nameServer, resource.getGraph())) {
			graph = this.getGrafoURI(nameServer, resource.getGraph());
			String query = builder.getQuery(operacion, resource);
			if (query.isEmpty())
				throw new SparqlException(
						"The SPARQL sentence can´t be created correctly. Check if you specify the right resource or the parameters are correctly");

			else if (query.contains("Error:"))
				throw new SparqlException(this.getError(query));
			rdf = this.generarDocumento(server, graph, query, resource.getFormat());
			if (rdf == null)
				throw new SparqlException("The response is incorrect. Check the identifier of the resource is correct");

		}
		return rdf;
	}

	/**
	 * Metodo principal, que se encarga de realizar la consulta SPARQL y devolver el
	 * resultado en formato TURTLE o JSON
	 * 
	 * @param operacion
	 *            Operacion ha realizar
	 * @param formatoEspecial
	 *            booleano para indicar que estan en formato especial (debe ser una
	 *            cadena)
	 * @param resource
	 *            Parametros de la consulta
	 * @return resultad consulta
	 * @throws SparqlException
	 */
	public String querySPARQL(OperationType operacion, boolean formatoEspecial, Resource resource)
			throws SparqlException {
		String server = this.getServerGraph(resource.getGraph());
		if (server == null || server.isEmpty())
			throw new SparqlException("The graph dont exist");
		String nameServer = this.getNameServer(server);
		String uriGraph = this.getGrafoURI(nameServer, resource.getGraph());
		String rdf = null;
		QueryBuilder builder = new QueryBuilder();

		if (operacion.equals(OperationType.GET_GRAPHS)) {
			String query = builder.getQuery(operacion, resource);
			if (query.isEmpty())
				throw new SparqlException(
						"The SPARQL sentence can´t be created correctly. Check if you specify the right resource or the parameters are correctly");
			rdf = this.generarRespuesta(server, uriGraph, query, resource.getFormat());
			if (rdf == null)
				throw new SparqlException("The response is incorrect. Check the identifier of the resource is correct");
		} else if (this.isValidQuery(nameServer, resource.getGraph())) {
			String query = builder.getQuery(operacion, resource);
			if (query.isEmpty())
				throw new SparqlException(
						"The SPARQL sentence can´t be created correctly. Check if you specify the right resource or the parameters are correctly");

			else if (query.contains("Error:"))
				throw new SparqlException(this.getError(query));
			rdf = this.generarRespuesta(server, uriGraph, query, resource.getFormat());
			if (rdf.isEmpty())
				throw new SparqlException("The response is incorrect. Check the identifier of the resource is correct");
		}
		return rdf;
	}

	/**
	 * Obtiene todas las clases que estan definidas en el grafo
	 * 
	 * @param resource
	 *            Conjunto de parametros para la consulta
	 * @return clases
	 */
	private Map<String, String> getClasses(Resource resource) {
		Resource rs = null;
		try {
			rs = resource.clone();
		} catch (CloneNotSupportedException e1) {
			throw new SparqlException("Error to clone resource");
		}
		rs.setFormat("application/xml");
		Document classesDocument = this.querySPARQL(OperationType.GET_CLASSES, rs);
		// La clave es la URI de la clase y el valor es la etiqueta (Si tiene)
		Map<String, String> classes = new HashMap<String, String>();
		NodeList listOfClasses = null;
		try {
			listOfClasses = Utils.getNodes(classesDocument,
					"/sparql/results//result/binding[@name = \"class\"]/uri|/sparql/results//result/binding[@name = \"label\"]/literal");
		} catch (ConfigException e) {
			throw new SparqlException("Fail to get classes of the graph", e);
		}

		int length = listOfClasses.getLength();
		Node nodo = null;
		String uri = "";
		String label = "";
		for (int i = 0; i < length; i++) {
			nodo = listOfClasses.item(i);

			if (i > 0 && i % 2 == 0) {
				classes.put(uri, label);
				uri = "";
				label = "";
			}

			if (nodo.getNodeName().equals("uri"))
				uri = nodo.getTextContent();
			else if (nodo.getNodeName().equals("literal"))
				label = nodo.getTextContent();

		}
		// Si aun queda clases por añadir
		if (!uri.isEmpty()) {
			classes.put(uri, label);
			uri = "";
			label = "";
		}

		return classes;
	}

	/**
	 * Método para poner las clases que son consultables
	 * 
	 * @param path
	 *            Path de acceso
	 * @param grafo
	 *            Grafo
	 * @param ayuda
	 *            menu
	 * @throws SparqlException
	 */
	private void setClases(String servicio, XMLGregorianCalendar fecha, String path, String grafo, MenuElementos ayuda,
			String[] formatos, String[] describeFormats, Map<String, String> classes) throws SparqlException {

		String claseCodificada = "";

		for (String uriClase : classes.keySet()) {
			try {
				claseCodificada = uriClase.replaceAll("#", Utils.cadenaURLEncoder("#", "UTF-8"));
			} catch (ConfigException e) {
				throw new SparqlException("Fail to codify URL of the class", e);
			}
			ayuda.addClase(path + claseCodificada, claseCodificada, fecha, servicio, uriClase, classes.get(uriClase),
					path + claseCodificada, formatos, describeFormats);

		}

	}

	/**
	 * Realizar una consulta SPARQL para obtener las propiedades del grafo
	 * 
	 * @param graph
	 *            Grafo
	 * @param shorted
	 *            ordenado
	 * @return propiedades
	 */
	private Document getInfoProperties(Resource resource) {
		Document docProperties = null;

		String query = "SELECT DISTINCT ?property ?type\n" + "WHERE {\n" + "{\n" + "SELECT ?property ?type\n"
				+ "WHERE\n" + "{\n" + "?property rdf:type ?type .\n"
				+ "?type rdfs:subPropertyOf* owl:ObjectProperty .\n" + "}\n" + "} UNION {\n"
				+ "SELECT ?property ?type\n" + "WHERE\n" + "{\n" + "?property rdf:type ?type .\n"
				+ "?type rdfs:subPropertyOf* owl:DatatypeProperty .\n" + "\n" + "}\n" + "}\n" + "}\n";

		if (resource.isSorted())
			query += "ORDER BY ?property\n";

		if (resource.getLimit() > 0)
			query += "\nLIMIT " + Integer.toString(resource.getLimit());
		if (resource.getOffset() > 0)
			query += "\nOFFSET " + Integer.toString(resource.getOffset());

		String server = this.getServerGraph(resource.getGraph());
		String uriGraph = this.getGrafoURI(this.getNameServer(server), resource.getGraph());
		docProperties = this.generarDocumento(server, uriGraph, query, "application/xml");

		return docProperties;
	}

	/**
	 * Obtiene las propiedades definidas en el grafo
	 * 
	 * @param graph
	 *            Grafo
	 * @param shorted
	 *            ordenado
	 * @return mapa
	 */
	private Map<String, String> getProperties(Resource resource) {
		// La clave es la URI de la propiedad y su valor es el tipo que es
		Map<String, String> properties = new HashMap<String, String>();
		Document doc = this.getInfoProperties(resource);
		NodeList listOfProperties = null;
		try {
			listOfProperties = Utils.getNodes(doc, "/sparql/results//result/*");
		} catch (ConfigException e) {
			throw new SparqlException("Fail to get nodes of properties", e);
		}

		int length = listOfProperties.getLength();
		Node nodo = null;
		String property = "";
		String type = "";
		for (int i = 0; i < length; i++) {
			if (i > 0 && i % 2 == 0) {
				properties.put(property, type);
				property = type = "";
			}
			nodo = listOfProperties.item(i);
			if (nodo.getAttributes().getNamedItem("name").getTextContent().equals("property"))
				property = nodo.getTextContent();

			if (nodo.getAttributes().getNamedItem("name").getTextContent().equals("type"))
				type = nodo.getTextContent();
		}

		return properties;
	}

	/**
	 * Método para poner las propiedades que se pueden consultar
	 * 
	 * @param servicio
	 *            Nombre del servicio
	 * @param fecha
	 *            Fecha XML
	 * @param path
	 *            Path de acceso
	 * @param grafo
	 *            nombre del grafo
	 * @param validFormats
	 *            formatos validos
	 * @param describeFormats
	 *            formatos describe
	 * @param shorted
	 *            Menu ordenado
	 */
	private void setPropiedades(String servicio, XMLGregorianCalendar fecha, String path, String grafo,
			MenuElementos ayuda, String[] formatos, String[] describeFormats, Resource resource) {

		Map<String, String> properties = this.getProperties(resource);
		String tipoPropiedad = "";
		String propertyCodified = "";
		for (String property : properties.keySet()) {
			try {
				propertyCodified = property.replaceAll("#", Utils.cadenaURLEncoder("#", "UTF-8"));
			} catch (ConfigException e) {
				throw new SparqlException("Fail to codify the URL of the property", e);
			}
			tipoPropiedad = properties.get(property);
			tipoPropiedad = (tipoPropiedad.equals("http://www.w3.org/2002/07/owl#ObjectProperty") ? "owl:ObjectProperty"
					: "owl:DatatypeProperty");
			ayuda.addPropiedad(servicio, propertyCodified, fecha, servicio, property, path + propertyCodified,
					tipoPropiedad, formatos, describeFormats);
		}

	}

	/**
	 * Método para parsear el objeto a formato ATOM
	 * 
	 * @param obj
	 *            Objeto
	 * @return xml en ATOM
	 */
	private String parseToATOM(Object obj) {
		String atom = "";
		JAXBContext jaxbContext = null;
		MenuGrafos menuGrafos = null;
		MenuElementos menuConsultas = null;
		CatalogueInfo catalogueInfo = null;
		try {
			if (obj instanceof MenuGrafos) {
				jaxbContext = JAXBContext.newInstance(MenuGrafos.class);
				menuGrafos = (MenuGrafos) obj;

			} else if (obj instanceof MenuElementos) {
				menuConsultas = (MenuElementos) obj;
				jaxbContext = JAXBContext.newInstance(MenuElementos.class);
			} else {
				catalogueInfo = (CatalogueInfo) obj;
				jaxbContext = JAXBContext.newInstance(CatalogueInfo.class);
			}
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter sw = new StringWriter();
			if (menuGrafos != null)
				jaxbMarshaller.marshal(menuGrafos, sw);
			else if (menuConsultas != null)
				jaxbMarshaller.marshal(menuConsultas, sw);
			else
				jaxbMarshaller.marshal(catalogueInfo, sw);

			atom = sw.toString();
		} catch (JAXBException e) {
			throw new SparqlException("Fallo al generar el formato ATOM del menu de ayuda");
		}
		return atom;
	}

	/**
	 * Método para crear el menu de ayuda que muestra los grafos disponibles
	 * 
	 * @param servicio
	 *            Nombre del servicio
	 * 
	 * @param path
	 *            Path de acceso
	 * 
	 * @param format
	 *            Formato del menu
	 * 
	 * @return menu de entrada
	 */
	public String graphsMenu(String servicio, String path, String format) {

		if (!path.endsWith("/"))
			path += "/";

		MenuGrafos ayuda = null;
		String menuAyuda = "";
		ayuda = new MenuGrafos();
		ayuda.setId(servicio);
		ayuda.setAuthor(servicio);
		ayuda.setTitle("List of graphs");
		Date fechaActual = new Date();
		XMLGregorianCalendar fechaActualXML = null;
		try {
			fechaActualXML = Utils.createFechaXML(fechaActual);
			ayuda.setUpdated(fechaActualXML);
		} catch (ConfigException e) {
			throw new SparqlException("Error to create date in XMLGregorian", e);
		}
		ayuda.setAuthor(servicio);
		List<String> graphs = null;
		try {
			graphs = ControllerSPARQLConfig.getUniqueInstance().getGrafos();
		} catch (ConfigException e) {
			throw new SparqlException("Error to get graphs", e);
		}
		String pathGrafo = "";
		for (String graph : graphs) {
			String nameGraph = Utils.splitStringByChar(graph, "/");
			if (nameGraph.matches(".*\\..*")) {
				nameGraph = nameGraph.replaceAll("\\..*", "");
			}
			pathGrafo = path + nameGraph + "/";
			ayuda.add(pathGrafo, nameGraph, fechaActualXML, servicio, pathGrafo);
		}
		if (format.equals("application/hal+json")) {
			try {
				Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
				JsonParser jp = new JsonParser();
				JsonElement je = jp.parse(ayuda.toJSON());
				String jsonFormatted = gson.toJson(je);
				return jsonFormatted;
			} catch (JSONException e) {
				throw new SparqlException("Error to parse entry point to JSON", e);
			}
		}
		menuAyuda = this.parseToATOM(ayuda);
		return menuAyuda;
	}

	/**
	 * Método para obtener un menu de ayuda que muestra los elementos que son
	 * consultables del grafo
	 * 
	 * @param servicio
	 *            Nombre del servicio
	 * @param path
	 *            Path de acceso
	 * @param grafo
	 *            nombre del grafo
	 * @param validFormats
	 *            formatos validos
	 * @param describeFormats
	 *            formatos describe
	 * @param shorted
	 *            Menu ordenado
	 * @param limit
	 *            Limitar menu
	 * @param offset
	 *            Reposicionar la posición del menu
	 * @param format
	 *            formato del menu
	 * @return menu
	 */
	public String graphDataMenu(String servicio, String path, String grafo, String[] validFormats,
			String[] describeFormats, Resource resource) {
		MenuElementos menuConsultas = new MenuElementos();
		if (!path.endsWith("/"))
			path += "/";
		XMLGregorianCalendar fechaActualXML = null;
		try {
			fechaActualXML = Utils.createFechaXML(new Date());
		} catch (ConfigException e) {
			throw new SparqlException("Error to create date in XMLGregorian", e);
		}
		validFormats = Arrays.stream(validFormats).filter(v -> !v.equals("application/hal+json"))
				.toArray(size -> new String[size]);
		menuConsultas.setId(servicio);
		menuConsultas.setAuthor(servicio);
		menuConsultas.setTitle(servicio);
		menuConsultas.setUpdated(fechaActualXML);
		Map<String, String> classes = null;
		if (!path.contains("classes") && !path.contains("properties")) {
			classes = this.getClasses(resource);
			this.setClases(servicio, fechaActualXML, path + "classes/", grafo, menuConsultas, validFormats,
					describeFormats, classes);

			this.setClases(servicio, fechaActualXML, path + "classes/count/", grafo, menuConsultas, validFormats,
					describeFormats, classes);
			this.setClases(servicio, fechaActualXML, path + "describe/", grafo, menuConsultas, validFormats,
					describeFormats, classes);
			this.setPropiedades(servicio, fechaActualXML, path + "properties/", grafo, menuConsultas, validFormats,
					describeFormats, resource);
			this.setPropiedades(servicio, fechaActualXML, path + "properties/count/", grafo, menuConsultas,
					validFormats, describeFormats, resource);
			this.setPropiedades(servicio, fechaActualXML, path + "describe/", grafo, menuConsultas, validFormats,
					describeFormats, resource);

		} else if (!path.contains("classes")) {
			this.setPropiedades(servicio, fechaActualXML, path, grafo, menuConsultas, validFormats, describeFormats,
					resource);
			this.setPropiedades(servicio, fechaActualXML, path + "/count/", grafo, menuConsultas, validFormats,
					describeFormats, resource);
		} else {
			classes = this.getClasses(resource);
			this.setClases(servicio, fechaActualXML, path, grafo, menuConsultas, validFormats, describeFormats,
					classes);

			this.setClases(servicio, fechaActualXML, path + "/count/", grafo, menuConsultas, validFormats,
					describeFormats, classes);

		}

		if (resource.getFormat().equals("application/hal+json")) {
			boolean onlyClasses = path.contains("classes");
			boolean onlyProperties = path.contains("properties");
			try {
				Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
				JsonParser jp = new JsonParser();
				JsonElement je = jp.parse(menuConsultas.toJSON(onlyClasses, onlyProperties));
				String jsonFormatted = gson.toJson(je);
				return jsonFormatted;
			} catch (JSONException e) {
				throw new SparqlException("Error to create JSON menu", e);
			}
		}
		String consultas = this.parseToATOM(menuConsultas);

		return consultas;
	}

	// Sistema de registro
	/**
	 * Obtiene la URL del servidor del grafo
	 * 
	 * @param graph
	 *            nombre del grafo o URL
	 * @return URL del servidor
	 */
	private String getServerGraph(String graph) {
		if (graph == null || graph.isEmpty())
			return "";
		try {
			return ControllerSPARQLConfig.getUniqueInstance().getServerPoint(graph);
		} catch (ConfigException e) {
			throw new SparqlException("Error to get the URL of the SPARQL point of the graph", e);
		}
	}

	/**
	 * Comprueba si el grafo ha sido registrado previamente
	 * 
	 * @param nameServer
	 *            Nombre del servidor
	 * @param graph
	 *            URL del grafo
	 * @return true/false
	 */
	private boolean isGraphRegistered(String nameServer, String graph) {
		try {
			return ControllerSPARQLConfig.getUniqueInstance().isGraphRegistered(nameServer, graph);
		} catch (ConfigException e) {
			throw new SparqlException("Error to check if the graph exists", e);
		}
	}

	/**
	 * Obtiene el centro de la URL del servidor. Se usara como nombre
	 * 
	 * @param server
	 *            URL del servidor
	 * @return nombre del servidor
	 */
	private String getNameServer(String server) {
		if (server == null || server.isEmpty())
			return "";

		int posBarra = server.indexOf("/") + 2;
		int posBarraFinal = server.indexOf("/", posBarra);
		String nameServer = server.substring(posBarra, posBarraFinal).replaceAll("\\.", "_");
		int posPuerto = nameServer.indexOf(":");
		if (posPuerto >= 0) {
			nameServer = nameServer.substring(0, posPuerto);
		}
		return nameServer;
	}

	/**
	 * Registra un grafo en el API
	 * 
	 * @param graph
	 *            URL del grafo
	 * @param server
	 *            URL del servidor
	 * @return titulo del catalogo
	 */
	public String registerGraph(String graph, String server) {
		
		String nameServer = getNameServer(server);
		if (this.isGraphRegistered(nameServer, graph))
			return "EXISTE";

		try {
			return ControllerSPARQLConfig.getUniqueInstance().registerGraph(nameServer, graph, server);
		} catch (ConfigException e) {
			throw new SparqlException("Error to create the config file of the graph", e);
		}
	}

	/**
	 * Elimina el acceso de un grafo de la API
	 * 
	 * @param graph
	 *            Nombre del grafo
	 * @return true/false
	 */
	public boolean removeGraph(String graph) {
		try {

			return ControllerSPARQLConfig.getUniqueInstance().removeGraph(graph);
		} catch (ConfigException e) {
			throw new SparqlException("Error to remove graph of the system", e);
		}
	}

	// Catalogos

	/**
	 * Obtiene un listado en ATOM o HAL sobre los catalogos registrados en la API
	 * 
	 * @param path
	 *            Path de acceso
	 * @param format
	 *            formato del menu
	 * @return menu
	 */
	public String getListCatalogues(String path, String format) {
		List<String> catalogues = null;
		try {
			catalogues = ControllerSPARQLConfig.getUniqueInstance().getListCatalogues();
		} catch (ConfigException e) {
			throw new SparqlException("Error to get list of catalogues", e);
		}
		MenuGrafos listCatalogues = new MenuGrafos();
		String valueIdAuthor = "ServicioSparqlRest";
		listCatalogues.setId(valueIdAuthor);
		listCatalogues.setAuthor(valueIdAuthor);
		listCatalogues.setTitle("Links of catalogues");
		Date fechaActual = new Date();
		XMLGregorianCalendar fechaActualXML = null;
		try {
			fechaActualXML = Utils.createFechaXML(fechaActual);
			listCatalogues.setUpdated(fechaActualXML);
		} catch (ConfigException e) {
			throw new SparqlException("Error to create date in XMLGregorian", e);
		}
		String href = "";
		String queryVersion = "";
		int posVersion = -1;
		String titleWithoutVersion = "";
		for (String title : catalogues) {
			posVersion = title.indexOf(".");
			titleWithoutVersion = (posVersion >= 0 ? title.substring(0, posVersion) : title);
			if (posVersion < 0) {
				href = path + titleWithoutVersion;
				listCatalogues.add(valueIdAuthor, titleWithoutVersion, fechaActualXML, valueIdAuthor, href);
			} else {
				queryVersion = "?version=" + title.substring(posVersion + 1, title.length());
				href = path + titleWithoutVersion + queryVersion;
				listCatalogues.add(valueIdAuthor, titleWithoutVersion, fechaActualXML, valueIdAuthor, href);
			}
		}
		if (format.equals("application/hal+json")) {
			try {
				Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
				JsonParser jp = new JsonParser();
				JsonElement je = jp.parse(listCatalogues.toJSON());
				String jsonFormatted = gson.toJson(je);
				return jsonFormatted;
			} catch (JSONException e) {
				throw new SparqlException("Error to create JSON menu", e);
			}
		}
		String consultas = this.parseToATOM(listCatalogues);

		return consultas;
	}

	/**
	 * Registrar el catalogo
	 * 
	 * @param rdf
	 *            Stream de bytes del catalogo
	 * @param fileDetail
	 *            Información adicional del catalogo
	 * @return titulo del catalogo
	 */
	public String registerCatalogue(InputStream rdf, FormDataContentDisposition fileDetail) {
		try {
			return ControllerSPARQLConfig.getUniqueInstance().registerCatalogue(rdf, fileDetail);
		} catch (ConfigException e) {
			throw new SparqlException("Error to store catalogue", e);
		}

	}

	/**
	 * Obtiene el catalogo
	 * 
	 * @param title
	 *            Titulo del catalogo
	 * @param version
	 *            Version del catalogo
	 * 
	 * @param path
	 *            Path de acceso
	 * @return catalogo
	 */
	public Document getCatalogue(String title, String version, String path) {
		try {
			if (!path.endsWith("/"))
				path += "/";
			return ControllerSPARQLConfig.getUniqueInstance().getCatalogue(title, version, path);
		} catch (ConfigException e) {
			throw new SparqlException("Error to get catalogue", e);
		}
	}
	/**
	 * Elimina un catalogo del sistema
	 * @param title Titulo del catalogo
	 * @param version Version del catalogo
	 * @return true/false
	 */
	public boolean removeCatalogue(String title, String version) {
		return ControllerSPARQLConfig.getUniqueInstance().removeCatalogue(title, version);
	}

	/**
	 * Obtiene una instancia del catalogo. Especificamente su DESCRIBE
	 * 
	 * @param title
	 *            Titulo del catalogo
	 * @param instance
	 *            URI de la instancia
	 * @param format
	 *            Formato del DESCRIBE
	 * 
	 * @return descripción de la instancia
	 */
	public Document getInstanceByCatalogue(String title, String instance, String version, String format) {
		Document docInstance = null;
		String[] serverInfo = null;
		try {
			serverInfo = ControllerSPARQLConfig.getUniqueInstance().getConnectionCatalogue(title, version);
		} catch (ConfigException e) {
			throw new SparqlException("Error to get URL of the server and URI graph of the catalogue", e);
		}
		String server = serverInfo[0];
		String graph = serverInfo[1];
		QueryBuilder builder = new QueryBuilder();
		Resource resource = new Resource();
		resource.setUrl(instance);
		String query = builder.getQuery(OperationType.DESCRIBE_ELEMENT, resource);
		docInstance = this.generarDocumento(server, graph, query, format);
		return docInstance;
	}

	/**
	 * Obtiene una instancia del catalogo. Especificamente su DESCRIBE
	 * 
	 * @param title
	 *            Titulo del catalogo
	 * @param instance
	 *            URI de la instancia
	 * @param format
	 *            Formato del DESCRIBE
	 * @param special
	 *            Parametro diferenciador para saber si retornar un string
	 * @return descripción de la instancia
	 */
	public String getInstanceByCatalogue(String title, String instance, String version, String format,
			boolean special) {
		String docInstance = null;
		String[] serverInfo = null;
		try {
			serverInfo = ControllerSPARQLConfig.getUniqueInstance().getConnectionCatalogue(title, version);
		} catch (ConfigException e) {
			throw new SparqlException("Error to get URL of the server and URI graph of the catalogue", e);
		}
		String server = serverInfo[0];
		String graph = serverInfo[1];
		QueryBuilder builder = new QueryBuilder();
		Resource resource = new Resource();
		resource.setUrl(instance);
		String query = builder.getQuery(OperationType.DESCRIBE_ELEMENT, resource);
		docInstance = this.generarRespuesta(server, graph, query, format);
		return docInstance;
	}

	/**
	 * Obtiene el menu en ATOM o HAL sobre los todos los formatos que se puede
	 * obtener la descripcion de la instancia
	 * 
	 * @param instance
	 *            URI de la instancia
	 * @param path
	 *            Path de acceso
	 * @param format
	 *            Formato del menu
	 * @param formats
	 *            Formatos soportados
	 * @return menu ATOM o HAL
	 */
	public String getListLinks(String title, String instance, String version, String path, String format,
			String[] formats) {
		CatalogueInfo menu = new CatalogueInfo();
		String valueIdAuthor = "ServicioSparqlRest";
		menu.setId(valueIdAuthor);
		menu.setAuthor(valueIdAuthor);
		menu.setTitle("Links of Instance");
		Date fechaActual = new Date();
		XMLGregorianCalendar fechaActualXML = null;
		try {
			fechaActualXML = Utils.createFechaXML(fechaActual);
			menu.setUpdated(fechaActualXML);
		} catch (ConfigException e) {
			throw new SparqlException("Error to create date in XMLGregorian", e);
		}
		String href = "";
		String rel = "";
		String queryParam = "";
		for (String formato : formats) {
			rel = (formato.equals("text/turtle") ? "" : "alternate");
			queryParam = (formato.equals("text/turtle") ? "" : "?format=" + formato);
			queryParam = (!formato.contains("+") ? queryParam : "?format=" + formato.replaceAll("\\+", "%2b"));
			queryParam += (!version.isEmpty() ? (queryParam.isEmpty() ? "?version=" + version : "&version=" + version)
					: "");
			href = path + queryParam;
			menu.add(rel, href, formato);
		}
		if (format.equals("application/hal+json")) {
			try {
				Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
				JsonParser jp = new JsonParser();
				JsonElement je = jp.parse(menu.toJSON());
				String jsonFormatted = gson.toJson(je);
				return jsonFormatted;
			} catch (JSONException e) {
				throw new SparqlException("Error to create JSON menu", e);
			}
		}
		String consultas = this.parseToATOM(menu);

		return consultas;
	}

}
